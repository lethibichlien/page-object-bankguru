package pageUIs;

public class AbstractPageUI {
	
	//POM 8 - Account 07:
	public static final String NEW_CUSTOMER_LINK = "//a[text()='New Customer']";
	public static final String DEPOSIT_LINK = "//a[text()='Deposit']";
	public static final String MANAGER_LINK = "//a[text()='Manager']";
	public static final String LOGOUT_LINK = "//a[text()='Log out']";
	
	
	// 1 loctor đại diện cho các pages
	public static final String DYNAMIC_MENU_LINK ="//ul[@class='menusubnav']//a[text()='%s']";
	public static final String DYNAMIC_TEXTBOX ="//input[@name='%s']";
	public static final String DYNAMIC_TEXTAREA ="//textarea[@name='%s']";
	public static final String DYNAMIC_BUTTON ="//input[@name='%s']";
	public static final String DYNAMIC_RADIO_BUTTON="//input[@value='%s']";
	public static final String DYNAMIC_DROPDOWN_LIST ="//select[@name='%s']";
	public static final String DYNAMIC_ERROR_MESSAGE ="//td[contains(text(),'%s')]/following-sibling::td/label";
	public static final String DYNAMIC_PAGE_TITLE ="//p[@class='heading3'and text()='%s']";
	public static final String DYNAMIC_DATA_INTABLE ="//td[text()='%s']/following-sibling::td";
	
}
