package pageUIs;

public class NewCustomerPageUI {
	public static final String CUSTOMER_NAME_TEXTBOX = "//input[@name='name']";
	public static final String GENDER_MALE_RADIO = "//input[@value='m']";
	public static final String DATE_OF_BIRTH_TEXTBOX = "//input[@name='dob']";
	public static final String ADDRESS_TEXTBOX = "//textarea[@name='addr']";
	public static final String CITY_TEXTBOX = "//input[@name='city']";
	public static final String STATE_TEXTBOX = "//input[@name='state']";
	public static final String PIN_TEXTBOX = "//input[@name='pinno']";
	public static final String PHONE_TEXTBOX = "//input[@name='telephoneno']";
	public static final String EMAIL_TEXTBOX = "//input[@name='emailid']";
	public static final String PASSWORD_TEXTBOX = "//input[@name='password']";
	public static final String SUBMIT_BUTTON = "//input[@name='sub']";
	public static final String ADD_NEW_CUSTOMER_HEADINGTEXT = "//p[@class='heading3']";
	public static final String CUSTOMER_NAME_VALUE_TEXT = "//td[text()='Customer Name']/following-sibling::td";
	public static final String GENDER_MALE_VALUE_TEXT = "//td[text()='Gender']/following-sibling::td";
	public static final String BIRTH_VALUE_TEXT = "//td[text()='Birthdate']/following-sibling::td";
	public static final String ADDRESS_VALUE_TEXT = "//td[text()='Address']/following-sibling::td";
	public static final String CITY_VALUE_TEXT = "//td[text()='City']/following-sibling::td";
	public static final String STATE_VALUE_TEXT = "//td[text()='State']/following-sibling::td";
	public static final String PHONE_VALUE_TEXT = "//td[text()='Mobile No.']/following-sibling::td";
	public static final String PIN_VALUE_TEXT = "//td[text()='Pin']/following-sibling::td";
	public static final String EMAIL_VALUE_TEXT = "//td[text()='Email']/following-sibling::td";
	public static final String NEW_CUSTOMER_REGISTERED_HEADINGTEXT = "//p[@class='heading3' and text()='Customer Registered Successfully!!!']";
	public static final String CUSTOMNER_NAME_MESSAGE_TEXT = "//label[@id='message']";
	public static final String CUSTOMNER_ADDRESS_MESSAGE_TEXT = "//label[@id='message3']";
	public static final String CUSTOMNER_CITY_MESSAGE_TEXT = "//label[@id='message4']";
	public static final String CUSTOMNER_STATE_MESSAGE_TEXT = "//label[@id='message5']";
	public static final String CUSTOMNER_PIN_MESSAGE_TEXT = "//label[@id='message6']";
	public static final String CUSTOMNER_PHONE_MESSAGE_TEXT = "//label[@id='message7']";
	public static final String CUSTOMNER_EMAIL_MESSAGE_TEXT = "//label[@id='message9']";
	
	//POM 7: Action chain
	public static final String NEW_CUSTOMER_LINK = "//a[text()='New Customer']";

}
