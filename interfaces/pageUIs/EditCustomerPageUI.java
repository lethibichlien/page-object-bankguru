package pageUIs;

public class EditCustomerPageUI {
	public static final String EDIT_CUSTOMER_LINK = "//a[text()='Edit Customer']";
	public static final String CUSTOMER_ID_TEXTBOX = "//input[@name='cusid']";
	public static final String SUBMIT_BUTTON = "//input[@name='AccSubmit']";
	public static final String CUSTOMER_ID_MESSAGE_TEXT = "//label[@id='message14']";
	public static final String EDIT_CUSTOMER_HEADING_TEXT="//p[@class='heading3']";
	public static final String ADDRESS_MESSAGE_TEXT = "//label[@id='message3']";
	public static final String ADDRESS_TEXTBOX = "//textarea[@name='addr']";
	public static final String CITY_MESSAGE_TEXT = "//label[@id='message4']";
	public static final String CITY_TEXTBOX = "//input[@name='city']";
	public static final String STATE_TEXTBOX = "//input[@name='state']";
	public static final String STATE_MESSAGE_TEXT = "//label[@id='message5']";
	public static final String PIN_TEXTBOX = "//input[@name='pinno']";
	public static final String PIN_MESSAGE_TEXT = "//label[@id='message6']";
}
