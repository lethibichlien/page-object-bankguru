package com.bankguru.account;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class CustomerDataJson {
	public static CustomerDataJson get(String filename)throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(new File(filename), CustomerDataJson.class);
	}
	@JsonProperty("customerName")
	String customerName;
	@JsonProperty("gender")
	String gender;
	@JsonProperty("dateOfBirth")
	String dateOfBirth;
	@JsonProperty("addrress")
	String addrress;
	@JsonProperty("city")
	String city;
	@JsonProperty("state")
	String state;
	@JsonProperty("pin")
	String pin;
	@JsonProperty("phone")
	String phone;
	@JsonProperty("email")
	String email;
	@JsonProperty("password")
	String password;
	@JsonProperty("createSuccessMessage")
	String createSuccessMessage;
	
	public String getCustomerName() {
		return customerName;
	}
	public String getGender() {
		return gender;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public String getAddrress() {
		return addrress;
	}
	public String getCity() {
		return city;
	}
	public String getPhone() {
		return phone;
	}
	public String getEmail() {
		return email;
	}
	public String getPassword() {
		return password;
	}
	public String getState() {
		return state;
	}
	public String getPin() {
		return pin;
	}
	public String getCreateSuccessMessage() {
		return createSuccessMessage;
	}
	

}
