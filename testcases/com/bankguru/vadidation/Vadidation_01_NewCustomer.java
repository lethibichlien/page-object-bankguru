package com.bankguru.vadidation;

import org.testng.annotations.Test;

import commons.AbstractPage;
import pageObjects.HomePageObject;
import pageObjects.LoginPageObject;
import pageObjects.NewCustomerPageObject;
import pageObjects.RegisterPageObject;
import pageUIs.NewCustomerPageUI;

import org.testng.annotations.BeforeClass;
import java.util.Random;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;


public class Vadidation_01_NewCustomer{
	WebDriver driver;
	LoginPageObject loginPage;
	HomePageObject homePage;
	NewCustomerPageObject newCustomerPage;
	String username, password;
	
	@BeforeClass
	public void beforeClass() {
		System.setProperty("webdriver.chrome.driver", ".\\resources\\chromedriver.exe");
		driver = new ChromeDriver();
		
		loginPage = new LoginPageObject(driver);
		homePage = new HomePageObject(driver);
		newCustomerPage = new NewCustomerPageObject(driver);
		
		System.out.println("LOGIN - STEP: 1. Open Bankguru Aplication ");
		driver.get("http://demo.guru99.com/v4/");
		username = "mngr207231";
		password ="nutUmEb";
		
		System.out.println("LOGIN -STEP: 2. Input username/password");
		loginPage.inputUsernameToTextbox(username);
		loginPage.inputPassworkToTextbox(password);
		
		System.out.println("LOGIN -STEP: 3. Click LOGIN button");
		loginPage.clickToLoginButton();
		
		System.out.println("NEW_CUSTOMER - STEP: 4. Click new customer");
		homePage.clickToCreateNewCustomerPage();
	}

	@Test(enabled = false)
	public void TC_01_NameCanNotBeEmpty() throws Exception {
		
		
		System.out.println("VALIDATION_Name can not be empty - STEP: 1. Not input Name");
		newCustomerPage.clickToCustomerName();		
		newCustomerPage.pressTAB();
		System.out.println("VALIDATION_Name can not be empty - STEP: 2. Verify not input Name");
		Assert.assertEquals(newCustomerPage.getCustomnerNameMessage(), "Customer name must not be blank");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_02_Name_CanNotBeNumeric() throws Exception {
		System.out.println("VALIDATION_Name Can Not Be Numeric - STEP: 1. Input name contain number");
		newCustomerPage.inputToCustomerNameTextbox("1234name123");
		System.out.println("VALIDATION_Name Can Not Be Numeric - STEP: 2. Verify 'Numbers are not allowed' must be shown");
		Assert.assertEquals(newCustomerPage.getCustomnerNameMessage(), "Numbers are not allowed");
		Thread.sleep(2000);
		
	}
	@Test(enabled = false)
	public void TC_03_Name_CanNotSpecialChars() throws Exception{
		
		System.out.println("VALIDATION_Name Can Not pecial Chars - STEP: 1. Input name contain special character");
		newCustomerPage.inputToCustomerNameTextbox("name~!@#$%^&*()");
		System.out.println("VALIDATION_Name Can Not pecial Chars - STEP: 2. Verify 'Special characters are not allowed' must be shown");
		Assert.assertEquals(newCustomerPage.getCustomnerNameMessage(), "Special characters are not allowed");
		Thread.sleep(2000);
		
	}
	
	@Test(enabled = false)
	public void TC_04_Name_FirstCharBlankSpace() throws Exception{
		
		System.out.println("VALIDATION_First Char Blank Space - STEP: 1. Input name with first space character");
		newCustomerPage.inputToCustomerNameTextbox(" name");
		System.out.println("VALIDATION_NEW_CUSTOMER - STEP: 2. Verify 'First character can not have space' must be shown");
		Assert.assertEquals(newCustomerPage.getCustomnerNameMessage(), "First character can not have space");
		Thread.sleep(2000);
		
	}
	
	@Test(enabled = false)
	public void TC_05_Address_CanNotBeEmpty() throws Exception {
		System.out.println("VALIDATION_Address empty - STEP: 1. Not input Address");
		newCustomerPage.clickToAdress();		
		newCustomerPage.pressTAB();
		System.out.println("VALIDATION_Address can not be empty - STEP: 2. Verify not input Address");
		Assert.assertEquals(newCustomerPage.getCustomnerAddressMessage(), "Address Field must not be blank");
		Thread.sleep(2000);
		
	}
	
	@Test(enabled = false)
	public void TC_06_Address_FirstCharBlankSpace() throws Exception{
		
		System.out.println("VALIDATION_Address wiht First Char Blank Space - STEP: 1. Input name with first space character");
		newCustomerPage.inputToAddressTextarea(" address");
		System.out.println("VALIDATION_NEW_CUSTOMER - STEP: 2. Verify 'First character can not have space' must be shown");
		Assert.assertEquals(newCustomerPage.getCustomnerAddressMessage(), "First character can not have space");
		Thread.sleep(2000);
		
	}
	
	@Test(enabled = false)
	public void TC_07_City_CanNotBeEmpty() throws Exception {
		System.out.println("VALIDATION_City empty - STEP: 1. Not input City");
		newCustomerPage.clickToCity();		
		newCustomerPage.pressTAB();
		System.out.println("VALIDATION_State can not be empty - STEP: 2. Verify not input City");
		Assert.assertEquals(newCustomerPage.getCustomnerCityMessage(), "City Field must not be blank");
		Thread.sleep(2000);
		
	}
	
	@Test(enabled = false)
	public void TC_08_City_CanNotBeNumeric() throws Exception {
		System.out.println("VALIDATION_City Can Not Be Numeric - STEP: 1. Input City contain number");
		newCustomerPage.inputToCityTextbox("1234city123");
		System.out.println("VALIDATION_City Can Not Be Numeric - STEP: 2. Verify 'Numbers are not allowed' must be shown");
		Assert.assertEquals(newCustomerPage.getCustomnerCityMessage(), "Numbers are not allowed");
		Thread.sleep(2000);
		
	}
	
	@Test(enabled = false)
	public void TC_09_City_CanNotSpecialChars() throws Exception{
		
		System.out.println("VALIDATION_City Can Not pecial Chars - STEP: 1. Input name contain special character");
		newCustomerPage.inputToCityTextbox("name~!@#$%^&*()");
		System.out.println("VALIDATION_City Can Not pecial Chars - STEP: 2. Verify 'Special characters are not allowed' must be shown");
		Assert.assertEquals(newCustomerPage.getCustomnerCityMessage(), "Special characters are not allowed");
		Thread.sleep(2000);
		
	}
	
	@Test(enabled = false)
	public void TC_10_City_FirstCharBlankSpace() throws Exception{
		
		System.out.println("VALIDATION_City_First Char Blank Space - STEP: 1. Input name with first space character");
		newCustomerPage.inputToCityTextbox(" city");
		System.out.println("VALIDATION_City_First Char Blank Space - STEP: 2. Verify 'First character can not have space' must be shown");
		Assert.assertEquals(newCustomerPage.getCustomnerCityMessage(), "First character can not have space");
		Thread.sleep(2000);
		
	}
	
	@Test(enabled = false)
	public void TC_11_State_CanNotBeEmpty() throws Exception {
		System.out.println("VALIDATION_State empty - STEP: 1. Not input State");
		newCustomerPage.clickToState();		
		newCustomerPage.pressTAB();
		System.out.println("VALIDATION_State can not be empty - STEP: 2. Verify not input State");
		Assert.assertEquals(newCustomerPage.getCustomnerStateMessage(), "State must not be blank");
		Thread.sleep(2000);
		
	}
	
	@Test(enabled = false)
	public void TC_12_State_CanNotBeNumeric() throws Exception {
		System.out.println("VALIDATION_State Can Not Be Numeric - STEP: 1. Input State contain number");
		newCustomerPage.inputToStateTextbox("1234city123");
		System.out.println("VALIDATION_State Can Not Be Numeric - STEP: 2. Verify 'Numbers are not allowed' must be shown");
		Assert.assertEquals(newCustomerPage.getCustomnerStateMessage(), "Numbers are not allowed");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_13_State_CanNotSpecialChars() throws Exception {
		System.out.println("VALIDATION_State Can Not Be Special Characters - STEP: 1. Input State contain special characters");
		newCustomerPage.inputToStateTextbox("State~!@#$%^&*()");
		System.out.println("VALIDATION_State Can Not Be Special Characters - STEP: 2. Verify 'Special characters are not allowed");
		Assert.assertEquals(newCustomerPage.getCustomnerStateMessage(), "Special characters are not allowed");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_14_State_FirstCharBlankSpace() throws Exception {
		System.out.println("VALIDATION_State Can Not Be First Space - STEP: 1. Input State with first space");
		newCustomerPage.inputToStateTextbox(" State");
		System.out.println("VALIDATION_State Can Not Be First Space - STEP: 2. Verify 'Special characters are not allowed");
		Assert.assertEquals(newCustomerPage.getCustomnerStateMessage(), "First character can not have space");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_15_PIN_CanNotBeCharacters() throws Exception {
		System.out.println("VALIDATION_PIN Can Not Be Characters - STEP: 1. Input PIN contain number");
		newCustomerPage.inputToPINTextbox("pin123");
		System.out.println("VALIDATION_PIN Can Not Be Characters - STEP: 2. Verify 'Numbers are not allowed' must be shown");
		Assert.assertEquals(newCustomerPage.getCustomnerPinMessage(), "Characters are not allowed");
		Thread.sleep(2000);
	}
	
	
	@Test(enabled = false)
	public void TC_16_PIN_CanNotBeEmpty() throws Exception {
		System.out.println("VALIDATION_PIN empty - STEP: 1. Not input PIN");
		newCustomerPage.clickToPIN();		
		newCustomerPage.pressTAB();
		System.out.println("VALIDATION_PIN can not be empty - STEP: 2. Verify not input PIN");
		Assert.assertEquals(newCustomerPage.getCustomnerPinMessage(), "PIN Code must not be blank");
		Thread.sleep(2000);
	}
	
	
	@Test(enabled = false)
	public void TC_17_PIN_LessThan6Digits() throws Exception {
		System.out.println("VALIDATION_PIN - STEP: 1. Input PIN less than 6 digits");
		newCustomerPage.inputToPINTextbox("123");	
		System.out.println("VALIDATION_PIN - STEP: 2. Verify Pin message");
		Assert.assertEquals(newCustomerPage.getCustomnerPinMessage(), "PIN Code must have 6 Digits");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_18_PIN_NotSpecialChars() throws Exception {
		System.out.println("VALIDATION_PIN - STEP: 1. Input PIN contain special characters");
		newCustomerPage.inputToPINTextbox("~!23$%^*&()");	
		System.out.println("VALIDATION_PIN - STEP: 2. Verify Pin message");
		Assert.assertEquals(newCustomerPage.getCustomnerPinMessage(), "Special characters are not allowed");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_19_PIN_NotFistBlankSpace() throws Exception {
		System.out.println("VALIDATION_PIN - STEP: 1. Input PIN with first space");
		newCustomerPage.inputToPINTextbox(" 12345");	
		System.out.println("VALIDATION_PIN - STEP: 2. Verify PIN message");
		Assert.assertEquals(newCustomerPage.getCustomnerPinMessage(), "First character can not have space");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_20_PIN_NotBlankSpace() throws Exception {
		System.out.println("VALIDATION_PIN - STEP: 1. Input PIN contain space character");
		newCustomerPage.inputToPINTextbox("123 45");	
		System.out.println("VALIDATION_PIN - STEP: 2. Verify PIN message");
		Assert.assertEquals(newCustomerPage.getCustomnerPinMessage(), "Characters are not allowed");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_21_Phone_CanNotBeEmpty() throws Exception {
		System.out.println("VALIDATION_PHONE - STEP: 1. Not input Phone");
		newCustomerPage.clickToPhone();		
		newCustomerPage.pressTAB();
		System.out.println("VALIDATION_PHONE - STEP: 2. Verify Phone message");
		Assert.assertEquals(newCustomerPage.getCustomnerPhoneMessage(), "Mobile no must not be blank");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_22_Phone_NotFistBlankSpace() throws Exception {
		System.out.println("VALIDATION_PHONE - STEP: 1. Input phone with first space");
		newCustomerPage.inputToPhoneTextbox(" 123456789");	
		System.out.println("VALIDATION_PHONE - STEP: 2. Verify phone message");
		Assert.assertEquals(newCustomerPage.getCustomnerPhoneMessage(), "First character can not have space");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_23_Phone_NotBlankSpace() throws Exception {
		System.out.println("VALIDATION_PHONE - STEP: 1. Input phone with space character");
		newCustomerPage.inputToPhoneTextbox("1234 56789");	
		System.out.println("VALIDATION_PHONE - STEP: 2. Verify phone message");
		Assert.assertEquals(newCustomerPage.getCustomnerPhoneMessage(), "Characters are not allowed");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_24_Phone_NotSpecialChars() throws Exception {
		System.out.println("VALIDATION_PHONE - STEP: 1. Input phone with special characters");
		newCustomerPage.inputToPhoneTextbox("12~!#$%^&789");	
		System.out.println("VALIDATION_PHONE - STEP: 2. Verify phone message");
		Assert.assertEquals(newCustomerPage.getCustomnerPhoneMessage(), "Special characters are not allowed");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_25_Email_CanNotBeEmpty() throws Exception {
		System.out.println("VALIDATION_EMAIL - STEP: 1. Not input email");
		newCustomerPage.clickToEmail();		
		newCustomerPage.pressTAB();
		System.out.println("VALIDATION_EMAIL - STEP: 2. Verify email message");
		Assert.assertEquals(newCustomerPage.getCustomnerEmailMessage(), "Email-ID must not be blank");
		Thread.sleep(2000);
	}
	
	@Test(enabled = true)
	public void TC_26_Email_NotCorrectFormat() throws Exception {
		System.out.println("VALIDATION_EMAIL - STEP: 1. Input email not correct format");
		newCustomerPage.inputToEmailTextbox("guru99@gmail");	
		System.out.println("VALIDATION_EMAIL - STEP: 2. Verify email message");
		Assert.assertEquals(newCustomerPage.getCustomnerEmailMessage(), "Email-ID is not valid");
		Thread.sleep(1000);
		
		System.out.println("VALIDATION_EMAIL - STEP: 1. Input email not correct format");
		newCustomerPage.inputToEmailTextbox("guru99");	
		System.out.println("VALIDATION_EMAIL - STEP: 2. Verify email message");
		Assert.assertEquals(newCustomerPage.getCustomnerEmailMessage(), "Email-ID is not valid");
		Thread.sleep(1000);
		
		System.out.println("VALIDATION_EMAIL - STEP: 1. Input email not correct format");
		newCustomerPage.inputToEmailTextbox("guru99@");	
		System.out.println("VALIDATION_EMAIL - STEP: 2. Verify email message");
		Assert.assertEquals(newCustomerPage.getCustomnerEmailMessage(), "Email-ID is not valid");
		Thread.sleep(1000);
		
		System.out.println("VALIDATION_EMAIL - STEP: 1. Input email not correct format");
		newCustomerPage.inputToEmailTextbox("gurr99gmail.com");	
		System.out.println("VALIDATION_EMAIL - STEP: 2. Verify email message");
		Assert.assertEquals(newCustomerPage.getCustomnerEmailMessage(), "Email-ID is not valid");
		Thread.sleep(1000);
	}
	
	
	@Test(enabled = false)
	public void TC_27_Email_NotFirstBlankSpace() throws Exception {
		System.out.println("VALIDATION_EMAIL - STEP: 1. Input phone with space character");
		newCustomerPage.inputToEmailTextbox(" lien05@gmail.com");	
		System.out.println("VALIDATION_EMAIL - STEP: 2. Verify email message");
		Assert.assertEquals(newCustomerPage.getCustomnerEmailMessage(), "First character can not have space");
		Thread.sleep(2000);
	}
	
	
	
	
	
	public int randomDataTest(){
		Random random = new Random();
		return random.nextInt(9999);
	}

	@AfterClass(alwaysRun = true)
	public void afterClass() {
		driver.quit();
	}

}
