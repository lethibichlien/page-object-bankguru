package com.bankguru.vadidation;

import org.testng.annotations.Test;

import commons.AbstractPage;
import pageObjects.EditCustomerPageObject;
import pageObjects.HomePageObject;
import pageObjects.LoginPageObject;
import pageObjects.NewCustomerPageObject;
import pageObjects.RegisterPageObject;
import pageUIs.NewCustomerPageUI;

import org.testng.annotations.BeforeClass;
import java.util.Random;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;


public class Vadidation_01_EditCustomer{
	WebDriver driver;
	LoginPageObject loginPage;
	HomePageObject homePage;
	EditCustomerPageObject editCustomerPage;
	String username, password, customerId;
	
	@BeforeClass
	public void beforeClass() {
		System.setProperty("webdriver.chrome.driver", ".\\resources\\chromedriver.exe");
		driver = new ChromeDriver();
		
		loginPage = new LoginPageObject(driver);
		homePage = new HomePageObject(driver);
		editCustomerPage = new EditCustomerPageObject(driver);
		
		System.out.println("LOGIN - STEP: 1. Open Bankguru Aplication ");
		driver.get("http://demo.guru99.com/v4/");
		username = "mngr207231";
		password ="nutUmEb";
		customerId= "25490";
		
		System.out.println("LOGIN -STEP: 2. Input username/password");
		loginPage.inputUsernameToTextbox(username);
		loginPage.inputPassworkToTextbox(password);
		
		System.out.println("LOGIN -STEP: 3. Click LOGIN button");
		loginPage.clickToLoginButton();
		
		System.out.println("NEW_CUSTOMER - STEP: 4. Click edit customer");
		homePage.clickToEditCustomerPage();
	}

	@Test(enabled = false)
	public void TC_01_CustomerId_CanNotBeEmpty() throws Exception {
		
		
		System.out.println("VALIDATION_Edit Customer- STEP: 1. Not input customer id");
		editCustomerPage.clickToCustomerName();		
		editCustomerPage.pressTabFromCustomerId();
		System.out.println("VALIDATION_Edit Customer - STEP: 2. Verify not input customer id");
		Assert.assertEquals(editCustomerPage.getCustomnerIdMessage(), "Customer ID is required");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_02_CustomerId_CanNotBeCharacter() throws Exception {
		System.out.println("VALIDATION_Edit Customer - STEP: 1. Input customner id contain characters");
		editCustomerPage.inputToCustomerIdTextbox("1234acc123");
		System.out.println("VALIDATION_Edit Customer - STEP: 2. Verify customer id message");
		Assert.assertEquals(editCustomerPage.getCustomnerIdMessage(), "Characters are not allowed");
		Thread.sleep(2000);
		
	}
	
	@Test(enabled = false)
	public void TC_03_CustomerId_NotSpecialCharacter() throws Exception {
		System.out.println("VALIDATION_Edit Customer - STEP: 1. Input customner id contain special characters");
		editCustomerPage.inputToCustomerIdTextbox("123~!@#$%^");
		System.out.println("VALIDATION_Edit Customer - STEP: 2. Verify customer id message");
		Assert.assertEquals(editCustomerPage.getCustomnerIdMessage(), "Special characters are not allowed");
		Thread.sleep(2000);
	}
	
	@Test(enabled = true)
	public void TC_04_CustomerId_Valid() throws Exception {
		System.out.println("VALIDATION_Edit Customer - STEP: 1. Input valid customner id");
		editCustomerPage.inputToCustomerIdTextbox(customerId);
		System.out.println("VALIDATION_Edit Customer - STEP: 2. Click to submit");
		editCustomerPage.clickToSubmit();
		System.out.println("VALIDATION_Edit Customer - STEP: 3. Verify Edit customer heading message");
		Assert.assertEquals(editCustomerPage.getEditCustomnerHeadingMessage(), "Edit Customer");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_05_EmptyAdress() throws Exception {
		System.out.println("VALIDATION_Edit Customer - STEP: 1. Not input Adress");
		editCustomerPage.notInputToAddressTextbox();
		System.out.println("VALIDATION_Edit Customer - STEP: 2. Press TAB");
		editCustomerPage.pressTabFromAddress();
		System.out.println("VALIDATION_Edit Customer - STEP: 3. Verify Address message");
		Assert.assertEquals(editCustomerPage.getEditAddressCustomnerMessage(), "Address Field must not be blank");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_06_EmptyCity() throws Exception {
		System.out.println("VALIDATION_Edit Customer - STEP: 1. Not input Adress");
		editCustomerPage.notInputToCityTextbox();
		System.out.println("VALIDATION_Edit Customer - STEP: 2. Press TAB");
		editCustomerPage.pressTabFromCity();
		System.out.println("VALIDATION_Edit Customer - STEP: 3. Verify Address message");
		Assert.assertEquals(editCustomerPage.getEditCityCustomnerMessage(), "City Field must not be blank");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_07_City_NotNumberic() throws Exception {
		System.out.println("VALIDATION_Edit Customer - STEP: 1. Input to city contain numberics");
		editCustomerPage.inputToCityTextbox("1234city123");
		
		System.out.println("VALIDATION_Edit Customer - STEP: 2. Verify city message");
		Assert.assertEquals(editCustomerPage.getEditCityCustomnerMessage(), "Numbers are not allowed");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_08_City_NotSpecialChars() throws Exception {
		System.out.println("VALIDATION_Edit Customer - STEP: 1. Input to city contain special chars");
		editCustomerPage.inputToCityTextbox("City!@#$%^");
		
		System.out.println("VALIDATION_Edit Customer - STEP: 2. Verify city message");
		Assert.assertEquals(editCustomerPage.getEditCityCustomnerMessage(), "Special characters are not allowed");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_09_EmptyState() throws Exception {
		System.out.println("VALIDATION_Edit Customer - STEP: 1. Not input state");
		editCustomerPage.notInputToStateTextbox();
		System.out.println("VALIDATION_Edit Customer - STEP: 2. Press TAB");
		editCustomerPage.pressTabFromState();
		System.out.println("VALIDATION_Edit Customer - STEP: 3. Verify state message");
		Assert.assertEquals(editCustomerPage.getEditStateCustomnerMessage(), "State must not be blank");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_10_State_NotNumberic() throws Exception {
		System.out.println("VALIDATION_Edit Customer - STEP: 1. Input to state contain numberics");
		editCustomerPage.inputToStateTextbox("1234state123");
		
		System.out.println("VALIDATION_Edit Customer - STEP: 2. Verify state message");
		Assert.assertEquals(editCustomerPage.getEditStateCustomnerMessage(), "Numbers are not allowed");
		Thread.sleep(2000);
	}
	
	@Test(enabled = false)
	public void TC_11_State_NotSpecialChars() throws Exception {
		System.out.println("VALIDATION_Edit Customer - STEP: 1. Input to state contain special chars");
		editCustomerPage.inputToStateTextbox("State!#$%%^&&*");
		
		System.out.println("VALIDATION_Edit Customer - STEP: 2. Verify state message");
		Assert.assertEquals(editCustomerPage.getEditStateCustomnerMessage(), "Special characters are not allowed");
		Thread.sleep(2000);
	}
	
	@Test(enabled = true)
	public void TC_12_PIN_NotChars() throws Exception {
		System.out.println("VALIDATION_Edit Customer - STEP: 1. Input to pin contain chars");
		editCustomerPage.inputToPinTextbox("123pin12");
		
		System.out.println("VALIDATION_Edit Customer - STEP: 2. Verify pin message");
		Assert.assertEquals(editCustomerPage.getEditPinCustomnerMessage(), "Characters are not allowed");
		Thread.sleep(2000);
	}

	
	
	
	
	public int randomDataTest(){
		Random random = new Random();
		return random.nextInt(9999);
	}

	@AfterClass(alwaysRun = true)
	public void afterClass() {
		driver.quit();
	}

}
