package com.bankguru.commons;

import commons.AbstractTest;
import commons.PageGeneratorManager;
import pageObjects.DepositPageObject;
import pageObjects.HomePageObject;
import pageObjects.LoginPageObject;
import pageObjects.NewAccountPageObject;
import pageObjects.NewCustomerPageObject;
import pageObjects.RegisterPageObject;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import java.util.Random;
import org.openqa.selenium.WebDriver;


public class Account_common_01_CreateToSystem extends AbstractTest{
	WebDriver driver;
	@Parameters("browser")
	@BeforeTest
	public void beforeTest(String browserName) {
		driver = openMultiBrowser(browserName);
		
		emailValue="vwalker" + randomDataTest() + "@desdev.cn";
		
		loginPage = PageGeneratorManager.getLoginPage(driver);
		
		log.info("REGISTER - STEP: 1 Verify Login form is displayed");
		verifyTrue(loginPage.isLoginPageDisplayed());
		
		log.info("REGISTER - STEP: 2. Click 'Here' link");
		loginPageUrl = loginPage.getLoginPageUrl();
		registerPage=loginPage.clickToHereLink();
		//registerPage = new RegisterPageObject(driver);
		//registerPage =PageGeneratorManager.getRegisterPage(driver);
		
		log.info("REGISTER - STEP: 4. Input emailId into textbox");
		registerPage.inputEmailToTextbox(emailValue);
		
		log.info("REGISTER - STEP: 5. Click 'Submit' button");
		registerPage.clickToSubmitButton();
		
		log.info("REGISTER - STEP: 6. Get username/password");
		USER_ID_INFO = registerPage.getUsernamInformation();
		PASSWORD_INFO = registerPage.getPassworkInformation();
		closeBrowserAndDriver(driver);
	}

	


	
	
	public static String USER_ID_INFO, PASSWORD_INFO;
	LoginPageObject loginPage;
	RegisterPageObject registerPage;
	HomePageObject homePage;
	NewCustomerPageObject newCustomerPage;
	NewAccountPageObject newAccountPage;
	DepositPageObject depositPage;
	
	
	String  loginPageUrl, emailValue;
}
