package com.bankguru.account;

import org.testng.annotations.Test;

import commons.AbstractPage;
import commons.AbstractTest;
import commons.PageGeneratorManager;
import pageObjects.DepositPageObject;
import pageObjects.HomePageObject;
import pageObjects.LoginPageObject;
import pageObjects.NewAccountPageObject;
import pageObjects.NewCustomerPageObject;
import pageObjects.RegisterPageObject;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.util.Random;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;


public class Account_08_RegisterAndLogin_DynamicLocator_RestParameter extends AbstractTest{
	WebDriver driver;
	@Parameters("browser")
	@BeforeClass
	public void beforeClass(String browserName) {
		driver = openMultiBrowser(browserName);
		
		emailValue="vwalker" + randomDataTest() + "@desdev.cn";
		passwordValue="123456";
		
		loginPage = PageGeneratorManager.getLoginPage(driver);
		
		
	}

	@Test
	public void TC_01_RegisterToSystem() {
		
		
		System.out.println("REGISTER - STEP: 1. Click 'Here' link");
		loginPageUrl = loginPage.getLoginPageUrl();
		registerPage=loginPage.clickToHereLink();
		//registerPage = new RegisterPageObject(driver);
		//registerPage =PageGeneratorManager.getRegisterPage(driver);
		
		System.out.println("REGISTER - STEP: 2. Input emailId into textbox");
		registerPage.inputEmailToTextbox(emailValue);
		
		System.out.println("REGISTER - STEP: 3. Click 'Submit' button");
		registerPage.clickToSubmitButton();
		
		System.out.println("REGISTER - STEP: 4. Get username/password");
		username = registerPage.getUsernamInformation();
		password = registerPage.getPassworkInformation();
		
	}
	
	@Test
	public void TC_02_LoginToSystem() {
		System.out.println("LOGIN - STEP: 1. Open Login Page");
		loginPage=registerPage.openLoginPageUrl(loginPageUrl);
		//loginPage = new LoginPageObject(driver);
		//loginPage = PageGeneratorManager.getLoginPage(driver);

		System.out.println("LOGIN -STEP: 2. Input username/password");
		loginPage.inputUsernameToTextbox(username);
		loginPage.inputPassworkToTextbox(password);
		
		System.out.println("LOGIN -STEP: 3. Click LOGIN button");
		homePage=loginPage.clickToLoginButton();
		//homePage = new HomePageObject(driver);
		//homePage=PageGeneratorManager.getHomePage(driver);
		System.out.println("LOGIN -STEP: 4. Verify Welcome message displayed");
		Assert.assertTrue(homePage.isWelcomMessageDisplayed("Welcome To Manager's Page of Guru99 Bank"));
		
		System.out.println("LOGIN -STEP: 5. Verify User ID displayed");
		Assert.assertTrue(homePage.isUserIDDisplayed(username));
	}
	
	@Test
	public void TC_03_OpenMultiplePage(){
		//Cách 1: Return trong duy nhất 1 hàm openmunltipblePage với các trường hợp. Và khi gọi trong testcase thì ép kiểu.
		System.out.println("Action chain - STEP: 1. HomePage navigate New Customer Page");
		newCustomerPage = (NewCustomerPageObject) homePage.openMultiplePage(driver, "New Customer");
		
		System.out.println("Action chain - STEP: 2. New Customer Page navigate Deposit Page");
		depositPage = (DepositPageObject) newCustomerPage.openMultiplePage(driver, "Deposit");
		
		System.out.println("Action chain - STEP: 3. Deposit Page navigate New Customer Page");
		newCustomerPage = (NewCustomerPageObject) depositPage.openMultiplePage(driver, "New Customer");
		
		System.out.println("Action chain - STEP: 4. New Customer Page navigate Home Page");
		homePage = (HomePageObject) newCustomerPage.openMultiplePage(driver, "Manager");
		
		
	}
	
	@Test
	public void TC_04_OpenMultiplePages(){
		//Cách 2: Không cần Return trong hàm openmunltipblePages. Khi gọi không cần add kiểu.
		System.out.println("Action chain - STEP: 1. HomePage navigate New Customer Page");
		homePage.openMultiplePages(driver, "New Customer");
		newCustomerPage = PageGeneratorManager.getNewCustomerPage(driver);
		
		System.out.println("Action chain - STEP: 2. New Customer Page navigate Deposit Page");
		newCustomerPage.openMultiplePages(driver, "Deposit");
		depositPage = PageGeneratorManager.getDeopsitPage(driver);
		
		System.out.println("Action chain - STEP: 3. Deposit Page navigate New Customer Page");
		depositPage.openMultiplePages(driver, "New Customer");
		newCustomerPage = PageGeneratorManager.getNewCustomerPage(driver);
		
		System.out.println("Action chain - STEP: 4. New Customer Page navigate Home Page");
		newCustomerPage.openMultiplePages(driver, "Manager");
		homePage = PageGeneratorManager.getHomePage(driver);
	}
	
	
	public int randomDataTest(){
		Random random = new Random();
		return random.nextInt(9999);
	}

	@AfterClass(alwaysRun = true)
	public void afterClass() {
		driver.quit();
	}
	
	
	LoginPageObject loginPage;
	RegisterPageObject registerPage;
	HomePageObject homePage;
	NewCustomerPageObject newCustomerPage;
	NewAccountPageObject newAccountPage;
	DepositPageObject depositPage;
	
	
	String username, password, loginPageUrl;
	String customerNameValue,genderMaleValue, dateOfBirthValue, addressValue, cityValue, stateValue;
	String phoneValue,pinValue, emailValue, passwordValue;
}
