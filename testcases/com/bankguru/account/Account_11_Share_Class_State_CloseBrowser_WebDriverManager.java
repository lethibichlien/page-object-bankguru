package com.bankguru.account;

import org.testng.annotations.Test;

import com.bankguru.commons.Account_common_01_CreateToSystem;

import commons.AbstractPage;
import commons.AbstractTest;
import commons.PageGeneratorManager;
import pageObjects.DepositPageObject;
import pageObjects.HomePageObject;
import pageObjects.LoginPageObject;
import pageObjects.NewAccountPageObject;
import pageObjects.NewCustomerPageObject;
import pageObjects.RegisterPageObject;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.util.Random;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;


public class Account_11_Share_Class_State_CloseBrowser_WebDriverManager extends AbstractTest{
	WebDriver driver;
	@Parameters("browser")
	@BeforeClass
	public void beforeClass(String browserName) {
		driver = openMultiBrowser(browserName);
		
		loginPage = PageGeneratorManager.getLoginPage(driver);
		
		System.out.println("User info at test class: "+ Account_common_01_CreateToSystem.USER_ID_INFO);
		System.out.println("Password info at test class: "+ Account_common_01_CreateToSystem.PASSWORD_INFO);
		
		log.info("Pre-condition - STEP: 1. Verify Login Page");
		verifyTrue(loginPage.isLoginPageDisplayed());
		

		log.info("Pre-condition -STEP: 2. Input username/password");
		loginPage.inputUsernameToTextbox(Account_common_01_CreateToSystem.USER_ID_INFO);
		loginPage.inputPassworkToTextbox(Account_common_01_CreateToSystem.PASSWORD_INFO);
		
		log.info("Pre-condition -STEP: 3. Click LOGIN button");
		homePage=loginPage.clickToLoginButton();
		
		log.info("Pre-condition -STEP: 4. Verify Welcome message displayed");
		verifyTrue(homePage.isWelcomMessageDisplayed("Welcome To Manager's Page of Guru99 Bank"));
		
		log.info("Pre-condition -STEP: 5. Verify User ID displayed");
		verifyTrue(homePage.isUserIDDisplayed(Account_common_01_CreateToSystem.USER_ID_INFO));
		
	}

	
	@Test(enabled=true)
	public void TC_01_CheckShareClassState() {
		
		
	}
	
	


	@AfterClass(alwaysRun = true)
	public void afterClass() {
		closeBrowserAndDriver(driver);
	}
	
	
	LoginPageObject loginPage;
	RegisterPageObject registerPage;
	HomePageObject homePage;
	NewCustomerPageObject newCustomerPage;
	NewAccountPageObject newAccountPage;
	DepositPageObject depositPage;
	
	
}
