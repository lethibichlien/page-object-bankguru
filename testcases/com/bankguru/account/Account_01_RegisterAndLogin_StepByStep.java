package com.bankguru.account;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Account_01_RegisterAndLogin_StepByStep {
	WebDriver driver;
	String email, username, password, loginPageUrl;
	@BeforeClass
	public void beforeClass() {
		System.setProperty("webdriver.chrome.driver", ".\\resources\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://demo.guru99.com/v4/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		System.out.println("GET LINK URL: ");
		loginPageUrl = driver.getCurrentUrl();
		email = "automation" + randomDataTest() + "@gmail.com";
	}

	@Test
	public void TC_01_RegisterToSystem() {
		
		System.out.println("REGISTER - STEP 1: Click 'Here' link");
		driver.findElement(By.xpath("//a[text()='here']")).click();
		
		System.out.println("REGISTER - STEP 2: Input emailId into textbox");
		driver.findElement(By.name("emailid")).sendKeys(email);
		
		System.out.println("REGISTER - STEP 3: Click 'Submit' button");
		driver.findElement(By.name("btnLogin")).click();
		
		System.out.println("REGISTER - STEP 4: Get username/password");
		username = driver.findElement(By.xpath("//td[text()='User ID :']/following-sibling::td")).getText();
		password = driver.findElement(By.xpath("//td[text()='Password :']/following-sibling::td")).getText();
		
	}
	@Test
	public void TC_02_LoginToSystem() {
		System.out.println("LOGIN - STEP 1: Open Login Page");
		driver.get(loginPageUrl);

		System.out.println("LOGIN -STEP 2: Input username/password");
		driver.findElement(By.name("uid")).sendKeys(username);
		driver.findElement(By.name("password")).sendKeys(password);
		
		System.out.println("LOGIN -STEP 3: Click LOGIN button");
		driver.findElement(By.name("btnLogin")).click();
		
		System.out.println("LOGIN -STEP 4: Verify Welcome message displayed");
		String welcomMessage = driver.findElement(By.cssSelector("marquee")).getText();
		Assert.assertEquals(welcomMessage, "Welcome To Manager's Page of Guru99 Bank");
		
		System.out.println("LOGIN -STEP 5: Verify User ID displayed");
		Assert.assertTrue(driver.findElement(By.xpath("//td[text()='Manger Id : "+ username + "']")).isDisplayed());
	}
	@Test
	public void TC_03_RegisterToSystem() {
	}
	
	public int randomDataTest(){
		Random random = new Random();
		return random.nextInt(9999);
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
