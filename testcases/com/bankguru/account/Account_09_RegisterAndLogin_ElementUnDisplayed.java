package com.bankguru.account;

import org.testng.annotations.Test;

import commons.AbstractPage;
import commons.AbstractTest;
import commons.PageGeneratorManager;
import pageObjects.DepositPageObject;
import pageObjects.HomePageObject;
import pageObjects.LoginPageObject;
import pageObjects.NewAccountPageObject;
import pageObjects.NewCustomerPageObject;
import pageObjects.RegisterPageObject;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.util.Random;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;


public class Account_09_RegisterAndLogin_ElementUnDisplayed extends AbstractTest{
	WebDriver driver;
	@Parameters("browser")
	@BeforeClass
	public void beforeClass(String browserName) {
		driver = openMultiBrowser(browserName);
		
		emailValue="vwalker" + randomDataTest() + "@desdev.cn";
		passwordValue="123456";
		
		loginPage = PageGeneratorManager.getLoginPage(driver);
		
		
	}

	@Test
	public void TC_01_RegisterToSystem() {
		System.out.println("REGISTER - STEP: 1.1 Verify Login form is displayed");
		Assert.assertTrue(loginPage.isLoginPageDisplayed());
		
		System.out.println("REGISTER - STEP: 1.2 Verify HomePage is not displayed");
		Assert.assertTrue(loginPage.isHomePageUnDisplayed());
		
		
		
		System.out.println("REGISTER - STEP: 1. Click 'Here' link");
		loginPageUrl = loginPage.getLoginPageUrl();
		registerPage=loginPage.clickToHereLink();
		//registerPage = new RegisterPageObject(driver);
		//registerPage =PageGeneratorManager.getRegisterPage(driver);
		
		System.out.println("REGISTER - STEP: 2. Input emailId into textbox");
		registerPage.inputEmailToTextbox(emailValue);
		
		System.out.println("REGISTER - STEP: 3. Click 'Submit' button");
		registerPage.clickToSubmitButton();
		
		System.out.println("REGISTER - STEP: 4. Get username/password");
		username = registerPage.getUsernamInformation();
		password = registerPage.getPassworkInformation();
		
	}
	
	@Test(enabled=false)
	public void TC_02_LoginToSystem() {
		System.out.println("LOGIN - STEP: 1. Open Login Page");
		loginPage=registerPage.openLoginPageUrl(loginPageUrl);
		

		System.out.println("LOGIN -STEP: 2. Input username/password");
		loginPage.inputUsernameToTextbox(username);
		loginPage.inputPassworkToTextbox(password);
		
		System.out.println("LOGIN -STEP: 3. Click LOGIN button");
		homePage=loginPage.clickToLoginButton();
		//homePage = new HomePageObject(driver);
		//homePage=PageGeneratorManager.getHomePage(driver);
		
	}
	
	@Test(enabled=false)
	public void TC_03_CheckHomePage(){
		System.out.println("LOGIN -STEP: 4. Verify Welcome message displayed");
		Assert.assertTrue(homePage.isWelcomMessageDisplayed("Welcome To Manager's Page of Guru99 Bank"));
		
		System.out.println("LOGIN -STEP: 5. Verify User ID displayed");
		Assert.assertTrue(homePage.isUserIDDisplayed(username));
	}
	
	
	public int randomDataTest(){
		Random random = new Random();
		return random.nextInt(9999);
	}

	@AfterClass(alwaysRun = true)
	public void afterClass() {
		driver.quit();
	}
	
	
	LoginPageObject loginPage;
	RegisterPageObject registerPage;
	HomePageObject homePage;
	NewCustomerPageObject newCustomerPage;
	NewAccountPageObject newAccountPage;
	DepositPageObject depositPage;
	
	
	String username, password, loginPageUrl, emailValue, passwordValue;
}
