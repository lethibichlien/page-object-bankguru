package com.bankguru.account;

import org.testng.annotations.Test;

import commons.AbstractPage;
import commons.PageGeneratorManager;
import pageObjects.HomePageObject;
import pageObjects.LoginPageObject;
import pageObjects.NewCustomerPageObject;
import pageObjects.RegisterPageObject;

import org.testng.annotations.BeforeClass;
import java.util.Random;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;


public class Account_05_RegisterAndLogin_PageGenerator{
	WebDriver driver;
	String username, password, loginPageUrl;
	LoginPageObject loginPage;
	RegisterPageObject registerPage;
	HomePageObject homePage;
	NewCustomerPageObject newCustomerPage;
	String customerNameValue,genderMaleValue, dateOfBirthValue, addressValue, cityValue, stateValue;
	String phoneValue,pinValue, emailValue, passwordValue;
	
	@BeforeClass
	public void beforeClass() {
		System.setProperty("webdriver.chrome.driver", ".\\resources\\chromedriver.exe");
		driver = new ChromeDriver();
		System.out.println("Driver at the TestCase layer: "+driver.toString());
		
		
		
		
		
		customerNameValue="Valina Walker";
		genderMaleValue="male";
		dateOfBirthValue="1991-01-01";
		addressValue="22349 Main Drive";
		cityValue="Cleveland";
		stateValue="Ohio";
		phoneValue="4403064356";
		pinValue="441256";
		emailValue="vwalker" + randomDataTest() + "@desdev.cn";
		passwordValue="123456";
		
		System.out.println("PRE-CONDITION - STEP: 1. Open Bankguru Aplication ");
		driver.get("http://demo.guru99.com/v4/");
		//loginPage = new LoginPageObject(driver);
		loginPage = PageGeneratorManager.getLoginPage(driver);
		System.out.println("PRE-CONDITION - STEP: 2. Get Login Page Url ");
		loginPageUrl = loginPage.getLoginPageUrl();
	}

	@Test
	public void TC_01_RegisterToSystem() {
		
		System.out.println("REGISTER - STEP: 1. Click 'Here' link");
		registerPage=loginPage.clickToHereLink();
		//registerPage = new RegisterPageObject(driver);
		//registerPage =PageGeneratorManager.getRegisterPage(driver);
		
		System.out.println("REGISTER - STEP: 2. Input emailId into textbox");
		registerPage.inputEmailToTextbox(emailValue);
		
		System.out.println("REGISTER - STEP: 3. Click 'Submit' button");
		registerPage.clickToSubmitButton();
		
		System.out.println("REGISTER - STEP: 4. Get username/password");
		username = registerPage.getUsernamInformation();
		password = registerPage.getPassworkInformation();
		
	}
	
	@Test
	public void TC_02_LoginToSystem() {
		System.out.println("LOGIN - STEP: 1. Open Login Page");
		loginPage=registerPage.openLoginPageUrl(loginPageUrl);
		//loginPage = new LoginPageObject(driver);
		//loginPage = PageGeneratorManager.getLoginPage(driver);

		System.out.println("LOGIN -STEP: 2. Input username/password");
		loginPage.inputUsernameToTextbox(username);
		loginPage.inputPassworkToTextbox(password);
		
		System.out.println("LOGIN -STEP: 3. Click LOGIN button");
		homePage=loginPage.clickToLoginButton();
		//homePage = new HomePageObject(driver);
		//homePage=PageGeneratorManager.getHomePage(driver);
		System.out.println("LOGIN -STEP: 4. Verify Welcome message displayed");
		Assert.assertTrue(homePage.isWelcomMessageDisplayed("Welcome To Manager's Page of Guru99 Bank"));
		
		System.out.println("LOGIN -STEP: 5. Verify User ID displayed");
		Assert.assertTrue(homePage.isUserIDDisplayed(username));
	}
	
	@Test
	public void TC_03_CreateNewCustomer() throws Exception{
		System.out.println("NEW_CUSTOMER - STEP: 1. Open New Customer Page");
		newCustomerPage=homePage.clickToCreateNewCustomerPage();
		//newCustomerPage = new NewCustomerPageObject(driver);
		//newCustomerPage = PageGeneratorManager.getNewCustomerPage(driver);
		
		System.out.println("NEW_CUSTOMER - STEP: 2. Verify new customer page displayed");
		Assert.assertTrue(newCustomerPage.isNewCustomerPageDisplayed());
		
		System.out.println("NEW_CUSTOMER - STEP: 3. Input to customer name textbox");
		newCustomerPage.inputToCustomerNameTextbox(customerNameValue);
		
		System.out.println("NEW_CUSTOMER - STEP: 4. Click to genger with 'male' value");
		newCustomerPage.clickToMaleValue();
		
		System.out.println("NEW_CUSTOMER - STEP: 5. Input to date of birth textbox");
		newCustomerPage.inputToDateOfBirthTextbox(dateOfBirthValue);
		
		System.out.println("NEW_CUSTOMER - STEP: 6. Input to Address textarea");
		newCustomerPage.inputToAddressTextarea(addressValue);
		
		System.out.println("NEW_CUSTOMER - STEP: 7. Input to City textbox");
		newCustomerPage.inputToCityTextbox(cityValue);
		
		System.out.println("NEW_CUSTOMER - STEP: 8. Input to State textbox");
		newCustomerPage.inputToStateTextbox(stateValue);
		
		System.out.println("NEW_CUSTOMER - STEP: 9. Input to PIN textbox");
		newCustomerPage.inputToPINTextbox(pinValue);
		
		System.out.println("NEW_CUSTOMER - STEP: 10. Input to Phone textbox");
		newCustomerPage.inputToPhoneTextbox(phoneValue);
		
		System.out.println("NEW_CUSTOMER - STEP: 11. Input to Email textbox");
		newCustomerPage.inputToEmailTextbox(emailValue);
		
		System.out.println("NEW_CUSTOMER - STEP: 12. Input to Password textbox");
		newCustomerPage.inputToPasswordTextbox(passwordValue);
		
		System.out.println("NEW_CUSTOMER - STEP: 13. Click to Submit button");
		newCustomerPage.clickToSubmitButton();
		Thread.sleep(3000);
		System.out.println("NEW_CUSTOMER - STEP: 14. Verify 'Customer Registered Successfully!!!' message displayed");
		Assert.assertTrue(newCustomerPage.isCustomerRegisteredMessageDisplayed());
		
		System.out.println("NEW_CUSTOMER - STEP: 15. Verify all information displayed");
		Assert.assertEquals(newCustomerPage.getCustomerNameValueInTable(), customerNameValue);
		Assert.assertEquals(newCustomerPage.getGenderMaleValueInTable(),genderMaleValue);
		Assert.assertEquals(newCustomerPage.getDateOfBirthValueInTable(), dateOfBirthValue);
		Assert.assertEquals(newCustomerPage.getAddressValueInTable(), addressValue);
		Assert.assertEquals(newCustomerPage.getCityValueInTable(), cityValue);
		Assert.assertEquals(newCustomerPage.getStateValueInTable(), stateValue);
		Assert.assertEquals(newCustomerPage.getPinValueInTable(), pinValue);
		Assert.assertEquals(newCustomerPage.getPhoneValueInTable(), phoneValue);
		Assert.assertEquals(newCustomerPage.getEmailValueInTable(), emailValue);
		
		
		
		
		
	}
	public int randomDataTest(){
		Random random = new Random();
		return random.nextInt(9999);
	}

	@AfterClass(alwaysRun = true)
	public void afterClass() {
		driver.quit();
	}

}
