package com.bankguru.account;

import org.testng.annotations.Test;

import commons.AbstractPage;
import pageFactory.HomePageFactory;
import pageFactory.LoginPageFactory;
import pageFactory.NewCustomerPageFactory;
import pageFactory.RegisterPageFactory;

import org.testng.annotations.BeforeClass;
import java.util.Random;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;


public class Account_04_RegisterAndLogin_Selenium_PageFactory{
	WebDriver driver;
	String username, password, loginPageUrl;
	LoginPageFactory loginPage;
	RegisterPageFactory registerPage;
	HomePageFactory homePage;
	//NewCustomerPageFactory newCustomerPage;
	
	String customerNameValue,genderMaleValue, dateOfBirthValue, addressValue, cityValue, stateValue;
	String phoneValue,pinValue, emailValue, passwordValue;
	
	@BeforeClass
	public void beforeClass() {
		System.setProperty("webdriver.chrome.driver", ".\\resources\\chromedriver.exe");
		driver = new ChromeDriver();
		System.out.println("Driver at the TestCase layer: "+driver.toString());
		
		loginPage = new LoginPageFactory(driver);
		registerPage = new RegisterPageFactory(driver);
		homePage = new HomePageFactory(driver);
		
		
		customerNameValue="Valina Walker";
		genderMaleValue="male";
		dateOfBirthValue="1991-01-01";
		addressValue="22349 Main Drive";
		cityValue="Cleveland";
		stateValue="Ohio";
		phoneValue="4403064356";
		pinValue="441256";
		emailValue="vwalker" + randomDataTest() + "@desdev.cn";
		passwordValue="123456";
		
		System.out.println("PRE-CONDITION - STEP: 1. Open Bankguru Aplication ");
		driver.get("http://demo.guru99.com/v4/");
		
		System.out.println("PRE-CONDITION - STEP: 2. Get Login Page Url ");
		loginPageUrl = loginPage.getLoginPageUrl();
	}

	@Test
	public void TC_01_RegisterToSystem() {
		
		System.out.println("REGISTER - STEP: 1. Click 'Here' link");
		loginPage.clickToHereLink();
		
		System.out.println("REGISTER - STEP: 2. Input emailId into textbox");
		registerPage.inputEmailToTextbox(emailValue);
		
		System.out.println("REGISTER - STEP: 3. Click 'Submit' button");
		registerPage.clickToSubmitButton();
		
		System.out.println("REGISTER - STEP: 4. Get username/password");
		username = registerPage.getUsernamInformation();
		password = registerPage.getPassworkInformation();
		
	}
	
	@Test
	public void TC_02_LoginToSystem() {
		System.out.println("LOGIN - STEP: 1. Open Login Page");
		registerPage.openLoginPageUrl(loginPageUrl);

		System.out.println("LOGIN -STEP: 2. Input username/password");
		loginPage.inputUsernameToTextbox(username);
		loginPage.inputPassworkToTextbox(password);
		
		System.out.println("LOGIN -STEP: 3. Click LOGIN button");
		loginPage.clickToLoginButton();
		
		System.out.println("LOGIN -STEP: 4. Verify Welcome message displayed");
		Assert.assertTrue(homePage.isWelcomMessageDisplayed("Welcome To Manager's Page of Guru99 Bank"));
		
		System.out.println("LOGIN -STEP: 5. Verify User ID displayed");
		Assert.assertTrue(homePage.isUserIDDisplayed(username));
	}
	
	
	public int randomDataTest(){
		Random random = new Random();
		return random.nextInt(9999);
	}

	@AfterClass(alwaysRun = true)
	public void afterClass() {
		driver.quit();
	}

}
