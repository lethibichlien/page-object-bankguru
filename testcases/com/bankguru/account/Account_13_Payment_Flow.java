package com.bankguru.account;

import org.testng.annotations.Test;

import com.bankguru.commons.Account_common_01_CreateToSystem;

import commons.AbstractTest;
import commons.PageGeneratorManager;
import pageObjects.BalanceEnquiryPageObject;
import pageObjects.ChangePasswordPageObject;
import pageObjects.CustomisedStatementPageObject;
import pageObjects.DeleteAccountPageObject;
import pageObjects.DeleteCustomerPageObject;
import pageObjects.DepositPageObject;
import pageObjects.EditAccountPageObject;
import pageObjects.EditCustomerPageObject;
import pageObjects.FundTranferPageObject;
import pageObjects.HomePageObject;
import pageObjects.LoginPageObject;
import pageObjects.MiniStatementPageObject;
import pageObjects.NewAccountPageObject;
import pageObjects.NewCustomerPageObject;
import pageObjects.WithdrawalPageObject;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;


public class Account_13_Payment_Flow extends AbstractTest{
	WebDriver driver;
	String customerName, gender,dateOfBirth, addrress,city,state;
	String pin, phone, email, password;
	String customerID;
	
	@Parameters("browser")
	@BeforeClass
	public void beforeClass(String browserName) {
		driver = openMultiBrowser(browserName);
		loginPage = PageGeneratorManager.getLoginPage(driver);
		
		customerName ="AUTOMATION TESTING";
		gender ="male";
		dateOfBirth ="2000-02-02"; 
		addrress ="PO Box 911 8311 Duis Avenue"; 
		city ="Tampa"; 
		state = "FL";
		pin = "466250";
		phone = "4555442476";
		email = "automation" + randomDataTest() + "@gmail.com";
		password = "automation";
		
		System.out.println("User info at test class: "+ Account_common_01_CreateToSystem.USER_ID_INFO);
		System.out.println("Password info at test class: "+ Account_common_01_CreateToSystem.PASSWORD_INFO);
		
		log.info("Pre-condition - STEP: 1. Verify Login Page");
		verifyTrue(loginPage.isLoginPageDisplayed());
		

		log.info("Pre-condition -STEP: 2. Input username/password");
		loginPage.inputUsernameToTextbox(Account_common_01_CreateToSystem.USER_ID_INFO);
		loginPage.inputPassworkToTextbox(Account_common_01_CreateToSystem.PASSWORD_INFO);
		
		log.info("Pre-condition -STEP: 3. Click LOGIN button");
		homePage=loginPage.clickToLoginButton();
	
		
	}

	
	@Test
	public void Payment_01_CreatNewCustomerAccountAndGetCustomerID() {
		log.info("Payment_01 - STEP: 1. Open New Customer Page");
		homePage.openMultiplePages(driver, "New Customer");
		newCustomerPage = PageGeneratorManager.getNewCustomerPage(driver);
		
		log.info("Payment_01 -STEP: 2. Click all fields");
		newCustomerPage.inputToDymanicTextbox(driver, customerName, "name");
		newCustomerPage.clickToDymanicRadioButton(driver, "m");
		newCustomerPage.removeAtributeOfDateOfBirth(driver, "dob", "type");
		newCustomerPage.inputToDymanicTextbox(driver, dateOfBirth, "dob");
		newCustomerPage.inputToDymanicTextarea(driver, addrress, "addr");
		newCustomerPage.inputToDymanicTextbox(driver, city, "city");
		newCustomerPage.inputToDymanicTextbox(driver, state, "state");
		newCustomerPage.inputToDymanicTextbox(driver, pin, "pinno");
		newCustomerPage.inputToDymanicTextbox(driver, phone, "telephoneno");
		newCustomerPage.inputToDymanicTextbox(driver, email, "emailid");
		newCustomerPage.inputToDymanicTextbox(driver, password, "password");
		
		log.info("Payment_01 -STEP: 3. Click Submit Button");
		newCustomerPage.clickToDymanicButton(driver, "sub");
		
		log.info("Payment_01 -STEP: 4. Verify  mesage 'Customer Registered Successfully!!!'");
		verifyTrue(newCustomerPage.isDynamicPageTitle(driver,"Customer Registered Successfully!!!" ));
		
		log.info("Payment_01 -STEP: 5. Verify all feild in table");
		verifyEquals(newCustomerPage.isDynamicDataInTable(driver, "Customer Name"), customerName);
		verifyEquals(newCustomerPage.isDynamicDataInTable(driver, "Gender"), gender);
		verifyEquals(newCustomerPage.isDynamicDataInTable(driver, "Birthdate"), dateOfBirth);
		verifyEquals(newCustomerPage.isDynamicDataInTable(driver, "Address"), addrress);
		verifyEquals(newCustomerPage.isDynamicDataInTable(driver, "City"), city);
		verifyEquals(newCustomerPage.isDynamicDataInTable(driver, "State"), state);
		verifyEquals(newCustomerPage.isDynamicDataInTable(driver, "Pin"), pin);
		verifyEquals(newCustomerPage.isDynamicDataInTable(driver, "Mobile No."), phone);
		verifyEquals(newCustomerPage.isDynamicDataInTable(driver, "Email"), email);
		
		//Get Customer ID for next steps
		customerID = newCustomerPage.isDynamicDataInTable(driver, "Customer ID");
		
	}
	
	@Test
	public void Payment_02_EditCustomerAccountAndCheckEditSuccessful() {
		
		
	}
	
	@Test
	public void Payment_03_AddNewAccountAndGetAccountID() {
		
		
	}
	
	@Test
	public void Payment_04_EditAccounAndCheckTypeAccount() {
		
		
	}
	
	@Test
	public void Payment_05_TransferMoneyIntoCurrentAccontAndCheckAccountBalanceEuqual() {
		
		
	}
	
	@Test
	public void Payment_06_TransferTheMoneyIntoOtherAccountAndCheckAmounEqual() {
		
		
	}
	
	@Test
	public void Payment_07_CheckCurrentAccountBalanceEqual() {
		
		
	}
	
	@Test
	public void Payment_08_DeleteAllAcountAndCheckDeleteSuccess() {
		
		
	}
	
	@Test
	public void Payment_09_DeleteCustomerAndCheckDeleteSuccess() {
		
		
	}
	
	@Test
	public void Payment_10_DeleteCustomerAndCheckDeleteSuccess() {
		
		
	}
	
	 

	@AfterClass(alwaysRun = true)
	public void afterClass() {
		closeBrowserAndDriver(driver);
	}
	
	
	LoginPageObject loginPage;
	HomePageObject homePage;
	NewCustomerPageObject newCustomerPage;
	EditCustomerPageObject editCustomerPage;
	DeleteCustomerPageObject deleteCustomerPage;
	NewAccountPageObject newAccountPage;
	EditAccountPageObject editAccountPage;
	DeleteAccountPageObject deleteAccountPage;
	DepositPageObject depositPage;
	WithdrawalPageObject withdrawalPage;
	FundTranferPageObject fundTransferPage;
	ChangePasswordPageObject changePasswordPage;
	BalanceEnquiryPageObject balaceEnquiryPage;
	MiniStatementPageObject miniStatementPage;
	CustomisedStatementPageObject customiseStatementPage;
	
	
	
}
