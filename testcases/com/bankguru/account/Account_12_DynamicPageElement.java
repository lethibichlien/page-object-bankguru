package com.bankguru.account;

import org.testng.annotations.Test;

import com.bankguru.commons.Account_common_01_CreateToSystem;

import commons.AbstractTest;
import commons.PageGeneratorManager;
import pageObjects.HomePageObject;
import pageObjects.LoginPageObject;
import pageObjects.NewAccountPageObject;
import pageObjects.NewCustomerPageObject;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;


public class Account_12_DynamicPageElement extends AbstractTest{
	WebDriver driver;
	@Parameters("browser")
	@BeforeClass
	public void beforeClass(String browserName) {
		driver = openMultiBrowser(browserName);
		
		loginPage = PageGeneratorManager.getLoginPage(driver);
		
		System.out.println("User info at test class: "+ Account_common_01_CreateToSystem.USER_ID_INFO);
		System.out.println("Password info at test class: "+ Account_common_01_CreateToSystem.PASSWORD_INFO);
		
		log.info("Pre-condition - STEP: 1. Verify Login Page");
		verifyTrue(loginPage.isLoginPageDisplayed());
		

		log.info("Pre-condition -STEP: 2. Input username/password");
		loginPage.inputUsernameToTextbox(Account_common_01_CreateToSystem.USER_ID_INFO);
		loginPage.inputPassworkToTextbox(Account_common_01_CreateToSystem.PASSWORD_INFO);
		
		log.info("Pre-condition -STEP: 3. Click LOGIN button");
		homePage=loginPage.clickToLoginButton();
		
		log.info("Pre-condition -STEP: 4. Verify Welcome message displayed");
		verifyTrue(homePage.isWelcomMessageDisplayed("Welcome To Manager's Page of Guru99 Bank"));
		
		log.info("Pre-condition -STEP: 5. Verify User ID displayed");
		verifyTrue(homePage.isUserIDDisplayed(Account_common_01_CreateToSystem.USER_ID_INFO));
		
	}

	
	@Test
	public void TC_01_ValidateErrorMessageAtNewCustomerForm() {
		
		log.info("Validate-NewCustomer-STEP: 1. Open New Customer and verify");
		homePage.openMultiplePages(driver, "New Customer");
		newCustomerPage = PageGeneratorManager.getNewCustomerPage(driver);
		verifyTrue(newCustomerPage.isDynamicPageTitle(driver, "Add New Customer"));
		
		log.info("Validate-NewCustomer-STEP: 2. Click all fields (empty data) at New Customer Form");
		newCustomerPage.clickToDymanicTextbox(driver, "name");
		newCustomerPage.clickToDymanicTextbox(driver, "dob");
		newCustomerPage.clickToDymanicTextarea(driver, "addr");
		newCustomerPage.clickToDymanicTextbox(driver, "city");
		newCustomerPage.clickToDymanicTextbox(driver, "state");
		newCustomerPage.clickToDymanicTextbox(driver, "pinno");
		newCustomerPage.clickToDymanicTextbox(driver, "telephoneno");
		newCustomerPage.clickToDymanicTextbox(driver, "emailid");
		newCustomerPage.clickToDymanicTextbox(driver, "password");
		newCustomerPage.clickToDymanicButton(driver, "emailid");
		
		
		log.info("Validate-NewCustomer-STEP: 3. Verify all fields display error mesage '<xxx> must not be blank'");
		verifyEquals(newCustomerPage.getDynamicErrorMessage(driver, "Customer Name"), "Customer name must not be blank");
		verifyEquals(newCustomerPage.getDynamicErrorMessage(driver, "Date of Birth"), "Date Field must not be blank");
		verifyEquals(newCustomerPage.getDynamicErrorMessage(driver, "Address"), "Address Field must not be blank");
		verifyEquals(newCustomerPage.getDynamicErrorMessage(driver, "City"), "City Field must not be blank");
		verifyEquals(newCustomerPage.getDynamicErrorMessage(driver, "State"), "State must not be blank");
		verifyEquals(newCustomerPage.getDynamicErrorMessage(driver, "PIN"), "PIN Code must not be blank");
		verifyEquals(newCustomerPage.getDynamicErrorMessage(driver, "Mobile Number"), "Mobile no must not be blank");
		verifyEquals(newCustomerPage.getDynamicErrorMessage(driver, "E-mail"), "Email-ID must not be blank");
		verifyEquals(newCustomerPage.getDynamicErrorMessage(driver, "Password"), "Password must not be blank");
	}
	
	@Test
	public void TC_02_ValidateErrorMessageAtNewAccoutnForm() {
		
		log.info("Validate-NewAccount-STEP: 1. Open New Acount and verify");
		newCustomerPage.openMultiplePages(driver, "New Account");
		newAccountPage = PageGeneratorManager.getNewAccountPage(driver);
		verifyTrue(newAccountPage.isDynamicPageTitle(driver, "Add new account form"));
		
		log.info("Validate-NewAccount-STEP: 2. Click all fields (empty data) at New Customer Form");
		newAccountPage.clickToDymanicTextbox(driver, "cusid");
		newAccountPage.clickToDymanicTextbox(driver, "inideposit");
		newAccountPage.pressDynamicKeyTabToTextbox(driver, "inideposit");
		
		
		log.info("Validate-NewAccount-STEP: 3. Verify all fields display error mesage '<xxx> must not be blank'");
		verifyEquals(newAccountPage.getDynamicErrorMessage(driver, "Customer id"), "Customer ID is required");
		verifyEquals(newAccountPage.getDynamicErrorMessage(driver, "Initial deposit"), "Initial Deposit must not be blank");
		
	}

	@Test
	public void TC_03_CreatNewCustomerPage() {
		
		log.info("CreateNewCustomer-STEP: 1. Open New Customer Page and verify");
		newCustomerPage.openMultiplePages(driver, "New Account");
		newAccountPage = PageGeneratorManager.getNewAccountPage(driver);
		verifyTrue(newAccountPage.isDynamicPageTitle(driver, "Add new account form"));
		
		log.info("Validate-NewAccount-STEP: 2. Click all fields (empty data) at New Customer Form");
		newAccountPage.clickToDymanicTextbox(driver, "cusid");
		newAccountPage.clickToDymanicTextbox(driver, "inideposit");
		newAccountPage.pressDynamicKeyTabToTextbox(driver, "inideposit");
		
		
		log.info("Validate-NewAccount-STEP: 3. Verify all fields display error mesage '<xxx> must not be blank'");
		verifyEquals(newAccountPage.getDynamicErrorMessage(driver, "Customer id"), "Customer ID is required");
		verifyEquals(newAccountPage.getDynamicErrorMessage(driver, "Initial deposit"), "Initial Deposit must not be blank");
		
	}

	@AfterClass(alwaysRun = true)
	public void afterClass() {
		closeBrowserAndDriver(driver);
	}
	
	
	LoginPageObject loginPage;
	HomePageObject homePage;
	NewCustomerPageObject newCustomerPage;
	NewAccountPageObject newAccountPage;
	
	//update 16082019
}
