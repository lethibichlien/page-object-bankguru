package com.bankguru.account;

import org.testng.annotations.Test;

import com.bankguru.commons.Account_common_01_CreateToSystem;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import commons.AbstractTest;
import commons.PageGeneratorManager;
import pageObjects.HomePageObject;
import pageObjects.LoginPageObject;
import pageObjects.NewAccountPageObject;
import pageObjects.NewCustomerPageObject;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;


public class Account_15_ManageTestData_Json extends AbstractTest{
	WebDriver driver;
	@Parameters({"browser","CUSTOMER_PATH"})
	@BeforeClass
	public void beforeClass(String browserName, String dataFilePath) throws JsonParseException, JsonMappingException, IOException {
		driver = openMultiBrowser(browserName);
		customerData = CustomerDataJson.get(dataFilePath);
		loginPage = PageGeneratorManager.getLoginPage(driver);
		
		System.out.println("User info at test class: "+ Account_common_01_CreateToSystem.USER_ID_INFO);
		System.out.println("Password info at test class: "+ Account_common_01_CreateToSystem.PASSWORD_INFO);
		
		log.info("Pre-condition - STEP: 1. Verify Login Page");
		verifyTrue(loginPage.isLoginPageDisplayed());
		

		log.info("Pre-condition -STEP: 2. Input username/password");
		loginPage.inputUsernameToTextbox(Account_common_01_CreateToSystem.USER_ID_INFO);
		loginPage.inputPassworkToTextbox(Account_common_01_CreateToSystem.PASSWORD_INFO);
		
		log.info("Pre-condition -STEP: 3. Click LOGIN button");
		homePage=loginPage.clickToLoginButton();
		
		log.info("Pre-condition -STEP: 4. Verify Welcome message displayed");
		verifyTrue(homePage.isWelcomMessageDisplayed("Welcome To Manager's Page of Guru99 Bank"));
		
		log.info("Pre-condition -STEP: 5. Verify User ID displayed");
		verifyTrue(homePage.isUserIDDisplayed(Account_common_01_CreateToSystem.USER_ID_INFO));
		
	}

	
	
	@Test
	public void TC_01_CreatNewCustomerPage() {
		
		log.info("CreateNewCustomer-STEP: 1. Click to Create New Customer page");
		homePage.openMultiplePages(driver, "New Customer");
		newCustomerPage = PageGeneratorManager.getNewCustomerPage(driver);
		verifyTrue(newCustomerPage.isDynamicPageTitle(driver, "Add New Customer"));
		
		log.info("CreateNewCustomer-STEP: 2. input to all field at Add New Customer Form");
		newCustomerPage.inputToDymanicTextbox(driver, customerData.getCustomerName(), "name");
		newCustomerPage.clickToDymanicRadioButton(driver, customerData.getGender());
		newCustomerPage.inputToDymanicTextbox(driver, customerData.getDateOfBirth(), "dob");
		newCustomerPage.inputToDymanicTextarea(driver, customerData.getAddrress(), "addr");
		newCustomerPage.inputToDymanicTextbox(driver, customerData.getCity(), "city");
		newCustomerPage.inputToDymanicTextbox(driver, customerData.getState(), "state");
		newCustomerPage.inputToDymanicTextbox(driver, customerData.getPin(), "pinno");
		newCustomerPage.inputToDymanicTextbox(driver, customerData.getPhone(), "telephoneno");
		newCustomerPage.inputToDymanicTextbox(driver, customerData.getEmail(), "emailid");
		newCustomerPage.inputToDymanicTextbox(driver, customerData.getPassword(), "password");
		
		newCustomerPage.clickToDymanicButton(driver, "sub");
		
		log.info("CreateNewCustomer-STEP: 3. Verify  mesage 'Customer Registered Successfully!!!'");
		verifyTrue(newCustomerPage.isDynamicPageTitle(driver, customerData.getCreateSuccessMessage()));
		
		
	}

	@AfterClass(alwaysRun = true)
	public void afterClass() {
		closeBrowserAndDriver(driver);
	}
	
	
	LoginPageObject loginPage;
	HomePageObject homePage;
	NewCustomerPageObject newCustomerPage;
	NewAccountPageObject newAccountPage;
	CustomerDataJson customerData;
	
	//update 16082019
}
