package com.bankguru.account;

import org.testng.annotations.Test;

import commons.AbstractPage;
import commons.AbstractTest;
import commons.PageGeneratorManager;
import pageObjects.DepositPageObject;
import pageObjects.HomePageObject;
import pageObjects.LoginPageObject;
import pageObjects.NewCustomerPageObject;
import pageObjects.RegisterPageObject;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.util.Random;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;


public class Account_07_RegisterAndLogin_ActionChain_WebDriverCycle extends AbstractTest{
	WebDriver driver;
	@Parameters("browser")
	@BeforeClass
	public void beforeClass(String browserName) {
		driver = openMultiBrowser(browserName);
		
		emailValue="vwalker" + randomDataTest() + "@desdev.cn";
		passwordValue="123456";
		
		loginPage = PageGeneratorManager.getLoginPage(driver);
		
		
	}

	@Test
	public void TC_01_RegisterToSystem() {
		
		
		System.out.println("REGISTER - STEP: 1. Click 'Here' link");
		loginPageUrl = loginPage.getLoginPageUrl();
		registerPage=loginPage.clickToHereLink();
		//registerPage = new RegisterPageObject(driver);
		//registerPage =PageGeneratorManager.getRegisterPage(driver);
		
		System.out.println("REGISTER - STEP: 2. Input emailId into textbox");
		registerPage.inputEmailToTextbox(emailValue);
		
		System.out.println("REGISTER - STEP: 3. Click 'Submit' button");
		registerPage.clickToSubmitButton();
		
		System.out.println("REGISTER - STEP: 4. Get username/password");
		username = registerPage.getUsernamInformation();
		password = registerPage.getPassworkInformation();
		
	}
	
	@Test
	public void TC_02_LoginToSystem() {
		System.out.println("LOGIN - STEP: 1. Open Login Page");
		loginPage=registerPage.openLoginPageUrl(loginPageUrl);
		//loginPage = new LoginPageObject(driver);
		//loginPage = PageGeneratorManager.getLoginPage(driver);

		System.out.println("LOGIN -STEP: 2. Input username/password");
		loginPage.inputUsernameToTextbox(username);
		loginPage.inputPassworkToTextbox(password);
		
		System.out.println("LOGIN -STEP: 3. Click LOGIN button");
		homePage=loginPage.clickToLoginButton();
		//homePage = new HomePageObject(driver);
		//homePage=PageGeneratorManager.getHomePage(driver);
		System.out.println("LOGIN -STEP: 4. Verify Welcome message displayed");
		Assert.assertTrue(homePage.isWelcomMessageDisplayed("Welcome To Manager's Page of Guru99 Bank"));
		
		System.out.println("LOGIN -STEP: 5. Verify User ID displayed");
		Assert.assertTrue(homePage.isUserIDDisplayed(username));
	}
	
	@Test
	public void TC_03_OpenMultiplePage(){
		//Muốn mở từng page phải viết bằng đấy hàm mở page và return page --> Bị lặp đi lặp lại các UI và các hàm khi chuyen Page qua lai --> Đưa các hàm này vào AbstracPage, AbstractPageUI
		System.out.println("Action chain - STEP: 1. HomePage navigate New Customer Page");
		newCustomerPage = homePage.openNewCustomerPage(driver);
		
		System.out.println("Action chain - STEP: 2. New Customer Page navigate Deposit Page");
		depositPage = newCustomerPage.openDepositPage(driver);
		
		
		System.out.println("Action chain - STEP: 3. Deposit Page navigate New Customer Page");
		newCustomerPage=depositPage.openNewCustomerPage(driver);
		
		loginPage = newCustomerPage.openLogoutLink(driver);
		loginPage.inputUsernameToTextbox(username);
		loginPage.inputPassworkToTextbox(password);
		homePage=loginPage.clickToLoginButton();
		newCustomerPage = homePage.openNewCustomerPage(driver);
		
		System.out.println("Action chain - STEP: 4. New Customer Page navigate Home Page");
		homePage=newCustomerPage.openHomePage(driver);
		
		System.out.println("Action chain - STEP: 5. Home Page navigate Deposit Page");
		homePage.openDepositPage(driver);
	}
	
	public int randomDataTest(){
		Random random = new Random();
		return random.nextInt(9999);
	}

	@AfterClass(alwaysRun = true)
	public void afterClass() {
		driver.quit();
	}
	
	
	LoginPageObject loginPage;
	RegisterPageObject registerPage;
	HomePageObject homePage;
	NewCustomerPageObject newCustomerPage;
	DepositPageObject depositPage;
	
	String customerNameValue,genderMaleValue, dateOfBirthValue, addressValue, cityValue, stateValue;
	String phoneValue,pinValue, emailValue, passwordValue;
	String username, password, loginPageUrl;
}
