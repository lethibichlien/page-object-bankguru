package pageObjects;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;
import commons.PageGeneratorManager;
import pageUIs.LoginPageUI;

public class LoginPageObject extends  AbstractPage{
	WebDriver driver;
	
	public LoginPageObject(WebDriver mappingDriver){
		driver = mappingDriver;
	}

	public String getLoginPageUrl() {
		System.out.println("Driver at Login Page: "+driver.toString());
		return getCurrentUrl(driver);
	}

	public RegisterPageObject clickToHereLink() {
		waitForElementVisible(driver, LoginPageUI.HERE_LINK);
		clickToElement(driver, LoginPageUI.HERE_LINK);
		return PageGeneratorManager.getRegisterPage(driver);
		
	}

	public void inputUsernameToTextbox(String username) {
		waitForElementVisible(driver, LoginPageUI.USERID_TEXTBOX);
		sendkeysToElement(driver, LoginPageUI.USERID_TEXTBOX, username);
		
	}

	public void inputPassworkToTextbox(String password) {
		waitForElementVisible(driver, LoginPageUI.PASSWORD_TEXTBOX);
		sendkeysToElement(driver, LoginPageUI.PASSWORD_TEXTBOX, password);
		
	}

	public HomePageObject clickToLoginButton() {
		waitForElementVisible(driver, LoginPageUI.LOGIIN_BUTTON);
		clickToElement(driver, LoginPageUI.LOGIIN_BUTTON);
		return PageGeneratorManager.getHomePage(driver);
		
	}

	public boolean isLoginPageDisplayed() {
		waitForElementVisible(driver, LoginPageUI.LOGIN_FORM);
		return isControlDisplayed(driver, LoginPageUI.LOGIN_FORM);
	}

	

	public boolean isHomePageUnDisplayed() {
		waitForElementInVisible(driver, LoginPageUI.WELCOM_MESSAGE_TEXT);
		return isControlUnDisplayed(driver, LoginPageUI.WELCOM_MESSAGE_TEXT);
	}

}
