package pageObjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import commons.AbstractPage;
import commons.PageGeneratorManager;
import pageUIs.NewCustomerPageUI;

public class NewCustomerPageObject extends AbstractPage{
	WebDriver driver;

	public NewCustomerPageObject(WebDriver mappingDriver){
		driver = mappingDriver;
	}

	public boolean isNewCustomerPageDisplayed() {
		waitForElementVisible(driver, NewCustomerPageUI.ADD_NEW_CUSTOMER_HEADINGTEXT);
		return isControlDisplayed(driver, NewCustomerPageUI.ADD_NEW_CUSTOMER_HEADINGTEXT);
	}

	public void inputToCustomerNameTextbox(String customerName) {
		waitForElementVisible(driver, NewCustomerPageUI.CUSTOMER_NAME_TEXTBOX);
		clearDataInTextbox(driver, NewCustomerPageUI.CUSTOMER_NAME_TEXTBOX);
		sendkeysToElement(driver, NewCustomerPageUI.CUSTOMER_NAME_TEXTBOX, customerName);
		
	}

	public void clickToMaleValue() {
		waitForElementVisible(driver, NewCustomerPageUI.GENDER_MALE_RADIO);
		clickToElement(driver, NewCustomerPageUI.GENDER_MALE_RADIO);
		
	}

	public void inputToDateOfBirthTextbox(String dateOfBirth) {
		waitForElementVisible(driver, NewCustomerPageUI.DATE_OF_BIRTH_TEXTBOX);
		removeAttributeInDOM(driver, NewCustomerPageUI.DATE_OF_BIRTH_TEXTBOX, "type");
		sendkeysToElement(driver, NewCustomerPageUI.DATE_OF_BIRTH_TEXTBOX, dateOfBirth);
		
	}

	public void inputToAddressTextarea(String adress) {
		waitForElementVisible(driver, NewCustomerPageUI.ADDRESS_TEXTBOX);
		sendkeysToElement(driver, NewCustomerPageUI.ADDRESS_TEXTBOX, adress);
		
	}

	public void inputToCityTextbox(String city) {
		waitForElementVisible(driver, NewCustomerPageUI.CITY_TEXTBOX);
		sendkeysToElement(driver, NewCustomerPageUI.CITY_TEXTBOX, city);
		
	}

	public void inputToStateTextbox(String state) {
		waitForElementVisible(driver, NewCustomerPageUI.STATE_TEXTBOX);
		sendkeysToElement(driver, NewCustomerPageUI.STATE_TEXTBOX, state);
		
	}

	public void inputToPhoneTextbox(String phone) {
		waitForElementVisible(driver, NewCustomerPageUI.PHONE_TEXTBOX);
		sendkeysToElement(driver, NewCustomerPageUI.PHONE_TEXTBOX, phone);
		
	}

	public void inputToPINTextbox(String pin) {
		waitForElementVisible(driver, NewCustomerPageUI.PIN_TEXTBOX);
		sendkeysToElement(driver, NewCustomerPageUI.PIN_TEXTBOX, pin);
		
	}

	public void inputToEmailTextbox(String email) {
		waitForElementVisible(driver, NewCustomerPageUI.EMAIL_TEXTBOX);
		clearDataInTextbox(driver, NewCustomerPageUI.EMAIL_TEXTBOX);
		sendkeysToElement(driver, NewCustomerPageUI.EMAIL_TEXTBOX, email);
		
	}

	public void inputToPasswordTextbox(String password) {
		waitForElementVisible(driver, NewCustomerPageUI.PASSWORD_TEXTBOX);
		sendkeysToElement(driver, NewCustomerPageUI.PASSWORD_TEXTBOX, password);
		
	}

	public void clickToSubmitButton() {
		waitForElementVisible(driver, NewCustomerPageUI.SUBMIT_BUTTON);
		clickToElement(driver, NewCustomerPageUI.SUBMIT_BUTTON);
		
	}

	public boolean isCustomerRegisteredMessageDisplayed() {
		waitForElementVisible(driver, NewCustomerPageUI.NEW_CUSTOMER_REGISTERED_HEADINGTEXT);
		return isControlDisplayed(driver, NewCustomerPageUI.NEW_CUSTOMER_REGISTERED_HEADINGTEXT);
	}

	public String getCustomerNameValueInTable() {
		waitForElementVisible(driver, NewCustomerPageUI.CUSTOMER_NAME_VALUE_TEXT);
		return getTextElement(driver, NewCustomerPageUI.CUSTOMER_NAME_VALUE_TEXT);
	}

	public String getGenderMaleValueInTable() {
		waitForElementVisible(driver, NewCustomerPageUI.GENDER_MALE_VALUE_TEXT);
		return getTextElement(driver, NewCustomerPageUI.GENDER_MALE_VALUE_TEXT);
	}

	public String getDateOfBirthValueInTable() {
		waitForElementVisible(driver, NewCustomerPageUI.BIRTH_VALUE_TEXT);
		return getTextElement(driver, NewCustomerPageUI.BIRTH_VALUE_TEXT);
	}

	public String getAddressValueInTable() {
		waitForElementVisible(driver, NewCustomerPageUI.ADDRESS_VALUE_TEXT);
		return getTextElement(driver, NewCustomerPageUI.ADDRESS_VALUE_TEXT);
	}

	public String getCityValueInTable() {
		waitForElementVisible(driver, NewCustomerPageUI.CITY_VALUE_TEXT);
		return getTextElement(driver, NewCustomerPageUI.CITY_VALUE_TEXT);
	}

	public String getStateValueInTable() {
		waitForElementVisible(driver, NewCustomerPageUI.STATE_VALUE_TEXT);
		return getTextElement(driver, NewCustomerPageUI.STATE_VALUE_TEXT);
	}

	public String getPinValueInTable() {
		waitForElementVisible(driver, NewCustomerPageUI.PIN_VALUE_TEXT);
		return getTextElement(driver, NewCustomerPageUI.PIN_VALUE_TEXT);
	}

	public String getPhoneValueInTable() {
		waitForElementVisible(driver, NewCustomerPageUI.PHONE_VALUE_TEXT);
		return getTextElement(driver, NewCustomerPageUI.PHONE_VALUE_TEXT);
	}

	public String getEmailValueInTable() {
		waitForElementVisible(driver, NewCustomerPageUI.EMAIL_VALUE_TEXT);
		return getTextElement(driver, NewCustomerPageUI.EMAIL_VALUE_TEXT);
	}

	public void pressTAB() {
		sendKeyBoardToElement(driver, NewCustomerPageUI.CUSTOMER_NAME_TEXTBOX, Keys.TAB);
		
	}

	

	public String getCustomnerNameMessage() {
		waitForElementVisible(driver, NewCustomerPageUI.CUSTOMNER_NAME_MESSAGE_TEXT);
		return getTextElement(driver, NewCustomerPageUI.CUSTOMNER_NAME_MESSAGE_TEXT);
	}

	public void clickToCustomerName() {
		waitForElementVisible(driver, NewCustomerPageUI.CUSTOMER_NAME_TEXTBOX);
		clickToElement(driver, NewCustomerPageUI.CUSTOMER_NAME_TEXTBOX);
		
	}

	public String getCustomnerAddressMessage() {
		waitForElementVisible(driver, NewCustomerPageUI.CUSTOMNER_ADDRESS_MESSAGE_TEXT);
		return getTextElement(driver, NewCustomerPageUI.CUSTOMNER_ADDRESS_MESSAGE_TEXT);
	}

	public void clickToAdress() {
		waitForElementVisible(driver, NewCustomerPageUI.ADDRESS_TEXTBOX);
		clickToElement(driver, NewCustomerPageUI.ADDRESS_TEXTBOX);
		
	}

	public void clickToCity() {
		waitForElementVisible(driver, NewCustomerPageUI.CITY_TEXTBOX);
		clickToElement(driver, NewCustomerPageUI.CITY_TEXTBOX);
		
		
	}

	public String getCustomnerCityMessage() {
		waitForElementVisible(driver, NewCustomerPageUI.CUSTOMNER_CITY_MESSAGE_TEXT);
		return getTextElement(driver, NewCustomerPageUI.CUSTOMNER_CITY_MESSAGE_TEXT);
	}

	public void clickToState() {
		waitForElementVisible(driver, NewCustomerPageUI.STATE_TEXTBOX);
		clickToElement(driver, NewCustomerPageUI.STATE_TEXTBOX);
		
	}

	public String getCustomnerStateMessage() {
		waitForElementVisible(driver, NewCustomerPageUI.CUSTOMNER_STATE_MESSAGE_TEXT);
		return getTextElement(driver, NewCustomerPageUI.CUSTOMNER_STATE_MESSAGE_TEXT);
	}

	public String getCustomnerPinMessage() {
		waitForElementVisible(driver, NewCustomerPageUI.CUSTOMNER_PIN_MESSAGE_TEXT);
		return getTextElement(driver, NewCustomerPageUI.CUSTOMNER_PIN_MESSAGE_TEXT);
	}

	public void clickToPIN() {
		waitForElementVisible(driver, NewCustomerPageUI.PIN_TEXTBOX);
		clickToElement(driver, NewCustomerPageUI.PIN_TEXTBOX);
		
	}

	public void clickToPhone() {
		waitForElementVisible(driver, NewCustomerPageUI.PHONE_TEXTBOX);
		clickToElement(driver, NewCustomerPageUI.PHONE_TEXTBOX);
		
	}

	public String getCustomnerPhoneMessage() {
		waitForElementVisible(driver, NewCustomerPageUI.CUSTOMNER_PHONE_MESSAGE_TEXT);
		return getTextElement(driver, NewCustomerPageUI.CUSTOMNER_PHONE_MESSAGE_TEXT);
	}

	public void clickToEmail() {
		waitForElementVisible(driver, NewCustomerPageUI.EMAIL_TEXTBOX);
		clickToElement(driver, NewCustomerPageUI.EMAIL_TEXTBOX);
	}

	public String getCustomnerEmailMessage() {
		waitForElementVisible(driver, NewCustomerPageUI.CUSTOMNER_EMAIL_MESSAGE_TEXT);
		return getTextElement(driver, NewCustomerPageUI.CUSTOMNER_EMAIL_MESSAGE_TEXT);
	}

	
	/*public DepositPageObject openDepositPage() {
		waitForElementVisible(driver, NewCustomerPageUI.NEW_CUSTOMER_LINK);
		clickToElement(driver,NewCustomerPageUI.NEW_CUSTOMER_LINK);
		return PageGeneratorManager.getDeopsitPage(driver);
	}*/ 
	
}
