package pageObjects;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;
import commons.PageGeneratorManager;
import pageUIs.RegisterPageUI;

public class RegisterPageObject extends  AbstractPage{
	WebDriver driver;

	public RegisterPageObject(WebDriver mappingDriver){
		driver = mappingDriver;
	}
	public void inputEmailToTextbox(String email) {
		System.out.println("Driver at Register Page: "+driver.toString());
		waitForElementVisible(driver, RegisterPageUI.EMAIL_TEXTBOX);
		sendkeysToElement(driver, RegisterPageUI.EMAIL_TEXTBOX, email);
		
	}

	public void clickToSubmitButton() {
		waitForElementVisible(driver, RegisterPageUI.SUBMIT_BUTTON);
		clickToElement(driver, RegisterPageUI.SUBMIT_BUTTON);
		
	}

	public String getUsernamInformation() {
		waitForElementVisible(driver, RegisterPageUI.USERNAME_TEXT);
		return getTextElement(driver, RegisterPageUI.USERNAME_TEXT);
	}

	public String getPassworkInformation() {
		waitForElementVisible(driver, RegisterPageUI.PASSWORD_TEXT);
		return getTextElement(driver, RegisterPageUI.PASSWORD_TEXT);
	}

	public LoginPageObject openLoginPageUrl(String loginPageUrl) {
		openUrl(driver, loginPageUrl);
		return PageGeneratorManager.getLoginPage(driver);
		
	}

}
