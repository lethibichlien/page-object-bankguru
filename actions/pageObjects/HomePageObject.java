package pageObjects;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;
import commons.PageGeneratorManager;
import pageUIs.EditCustomerPageUI;
import pageUIs.HomePageUI;
import pageUIs.NewCustomerPageUI;

public class HomePageObject extends  AbstractPage{
	WebDriver driver;
	
	
	public HomePageObject(WebDriver mappingDriver){
		driver = mappingDriver;
	}
	public boolean isWelcomMessageDisplayed(String string) {
		System.out.println("Driver at Home  Page: "+driver.toString());
		return isControlDisplayed(driver, HomePageUI.WELCOM_MESSAGE_TEXT);
	}

	public boolean isUserIDDisplayed(String username) {
		String actualText = getTextElement(driver, HomePageUI.USSERID_TEXT);
		return actualText.contains(username);
	}
	public NewCustomerPageObject clickToCreateNewCustomerPage() {
		waitForElementVisible(driver, HomePageUI.NEW_CUSTOMER_LINK);
		clickToElement(driver, HomePageUI.NEW_CUSTOMER_LINK);
		return PageGeneratorManager.getNewCustomerPage(driver);
		
	}
	public void clickToEditCustomerPage() {
		waitForElementVisible(driver, EditCustomerPageUI.EDIT_CUSTOMER_LINK);
		clickToElement(driver, EditCustomerPageUI.EDIT_CUSTOMER_LINK);
		
	}
	/*public NewCustomerPageObject openNewCustomerPage() {
		waitForElementVisible(driver, HomePageUI.NEW_CUSTOMER_LINK);
		clickToElement(driver,HomePageUI.NEW_CUSTOMER_LINK);
		return PageGeneratorManager.getNewCustomerPage(driver);
	}*/

}
