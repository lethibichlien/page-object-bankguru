package pageObjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import commons.AbstractPage;
import pageUIs.EditCustomerPageUI;
import pageUIs.NewCustomerPageUI;



public class EditCustomerPageObject extends AbstractPage {
	WebDriver driver;
	
	
	public EditCustomerPageObject(WebDriver mappingDriver){
		driver = mappingDriver;
	}


	public void clickToCustomerName() {
		waitForElementVisible(driver, EditCustomerPageUI.CUSTOMER_ID_TEXTBOX);
		clickToElement(driver, EditCustomerPageUI.CUSTOMER_ID_TEXTBOX);
		
	}


	public void pressTabFromCustomerId() {
		sendKeyBoardToElement(driver, EditCustomerPageUI.CUSTOMER_ID_TEXTBOX, Keys.TAB);
		
	}


	public String getCustomnerIdMessage() {
		waitForElementVisible(driver, EditCustomerPageUI.CUSTOMER_ID_MESSAGE_TEXT);
		return getTextElement(driver, EditCustomerPageUI.CUSTOMER_ID_MESSAGE_TEXT);
	}


	public void inputToCustomerIdTextbox(String customerId) {
		waitForElementVisible(driver, EditCustomerPageUI.CUSTOMER_ID_TEXTBOX);
		clearDataInTextbox(driver, EditCustomerPageUI.CUSTOMER_ID_TEXTBOX);
		sendkeysToElement(driver, EditCustomerPageUI.CUSTOMER_ID_TEXTBOX, customerId);
		
	}


	public void clickToSubmit() {
		waitForElementVisible(driver, EditCustomerPageUI.SUBMIT_BUTTON);
		clickToElement(driver, EditCustomerPageUI.SUBMIT_BUTTON);
		
	}


	public String getEditCustomnerHeadingMessage() {
		waitForElementVisible(driver, EditCustomerPageUI.EDIT_CUSTOMER_HEADING_TEXT);
		return getTextElement(driver, EditCustomerPageUI.EDIT_CUSTOMER_HEADING_TEXT);
	}


	public void notInputToAddressTextbox() {
		waitForElementVisible(driver, EditCustomerPageUI.ADDRESS_TEXTBOX);
		clearDataInTextbox(driver, EditCustomerPageUI.ADDRESS_TEXTBOX);
		
	}


	public void pressTabFromAddress() {
		sendKeyBoardToElement(driver, EditCustomerPageUI.ADDRESS_TEXTBOX, Keys.TAB);
		
	}


	public String getEditAddressCustomnerMessage() {
		waitForElementVisible(driver, EditCustomerPageUI.ADDRESS_MESSAGE_TEXT);
		return getTextElement(driver, EditCustomerPageUI.ADDRESS_MESSAGE_TEXT);
	}


	public void notInputToCityTextbox() {
		waitForElementVisible(driver, EditCustomerPageUI.CITY_TEXTBOX);
		clearDataInTextbox(driver, EditCustomerPageUI.CITY_TEXTBOX);
		
		
	}


	public void pressTabFromCity() {
		sendKeyBoardToElement(driver, EditCustomerPageUI.CITY_TEXTBOX, Keys.TAB);
		
	}


	public String getEditCityCustomnerMessage() {
		waitForElementVisible(driver, EditCustomerPageUI.CITY_MESSAGE_TEXT);
		return getTextElement(driver, EditCustomerPageUI.CITY_MESSAGE_TEXT);
	}


	public void inputToCityTextbox(String city) {
		waitForElementVisible(driver, EditCustomerPageUI.CITY_TEXTBOX);
		clearDataInTextbox(driver, EditCustomerPageUI.CITY_TEXTBOX);
		sendkeysToElement(driver, EditCustomerPageUI.CITY_TEXTBOX, city);
		
	}


	public void notInputToStateTextbox() {
		waitForElementVisible(driver, EditCustomerPageUI.STATE_TEXTBOX);
		clearDataInTextbox(driver, EditCustomerPageUI.STATE_TEXTBOX);
		
		
	}


	public void pressTabFromState() {
		sendKeyBoardToElement(driver, EditCustomerPageUI.STATE_TEXTBOX, Keys.TAB);
		
	}


	public String getEditStateCustomnerMessage() {
		waitForElementVisible(driver, EditCustomerPageUI.STATE_MESSAGE_TEXT);
		return getTextElement(driver, EditCustomerPageUI.STATE_MESSAGE_TEXT);
	}


	public void inputToStateTextbox(String state) {
		waitForElementVisible(driver, EditCustomerPageUI.STATE_TEXTBOX);
		clearDataInTextbox(driver, EditCustomerPageUI.STATE_TEXTBOX);
		sendkeysToElement(driver, EditCustomerPageUI.STATE_TEXTBOX, state);
		
	}


	public void inputToPinTextbox(String pin) {
		waitForElementVisible(driver, EditCustomerPageUI.PIN_TEXTBOX);
		clearDataInTextbox(driver, EditCustomerPageUI.PIN_TEXTBOX);
		sendkeysToElement(driver, EditCustomerPageUI.PIN_TEXTBOX, pin);
		
	}


	public String getEditPinCustomnerMessage() {
		waitForElementVisible(driver, EditCustomerPageUI.PIN_MESSAGE_TEXT);
		return getTextElement(driver, EditCustomerPageUI.PIN_MESSAGE_TEXT);
	}


	
}
