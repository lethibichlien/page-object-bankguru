package pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pageUIs.EditCustomerPageUI;
import pageUIs.HomePageUI;

public class HomePageFactory {
	private WebDriver driver;
	@FindBy(how=How.XPATH, using="//marquee")
	private WebElement welcomMessageText;
	@FindBy(how=How.XPATH, using="//tr[@class='heading3']//child::td")
	private WebElement userIdText;
	@FindBy(how=How.XPATH, using="//a[text()='New Customer']")
	private WebElement newCustomerLink;
	public HomePageFactory(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	public boolean isWelcomMessageDisplayed(String string) {
		return welcomMessageText.isDisplayed();
	}
	public boolean isUserIDDisplayed(String username) {
		String actualText = userIdText.getText().trim();
		return actualText.contains(username);
	}
	public void clickToCreateNewCustomerPage() {
		newCustomerLink.click();
		
	}
	
	
}
