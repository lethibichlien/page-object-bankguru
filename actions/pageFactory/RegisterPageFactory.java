package pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pageUIs.RegisterPageUI;

public class RegisterPageFactory {
	private WebDriver driver;
	@FindBy(how=How.XPATH, using="//input[@name='emailid']")
	private WebElement emailTextbox;
	@FindBy(how=How.XPATH, using="//input[@name='btnLogin']")
	private WebElement submitButton;
	@FindBy(how=How.XPATH, using="//td[text()='User ID :']/following-sibling::td")
	private WebElement usernameTextbox;
	@FindBy(how=How.XPATH, using="//td[text()='Password :']/following-sibling::td")
	private WebElement passwordTextbox;
	public RegisterPageFactory(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	public void inputEmailToTextbox(String email) {
		emailTextbox.sendKeys(email);
		
	}
	public void clickToSubmitButton() {
		submitButton.click();
	}
	public String getUsernamInformation() {
		return usernameTextbox.getText();
	}
	public String getPassworkInformation() {
		return passwordTextbox.getText();
	}
	public void openLoginPageUrl(String loginPageUrl) {
		driver.get(loginPageUrl);
		
	}


}
